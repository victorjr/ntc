<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Recertifications extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();
    var $tbl = 'recertification';
    var $fields = 'id,firstName,lastName,gender,age,currentFieldOfWork';

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()
    {
        $limit = 20;
        $conditions = array(
            'table'=>$this->tbl,
            'order'=>$this->tbl.".id DESC"
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "firstName like '%".$q."%' OR lastName like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/job_corps/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/recertifications',$data);
        $this->load->view('admin/template/footer');

    }

    
    public function addRecertification(){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            // echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('firstName', 'First Name', 'required');            
            $this->form_validation->set_rules('lastName', 'Last Name', 'required');
            $this->form_validation->set_rules('age', 'Age', 'required');            
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('currentFieldOfWork', 'Current Field of Work', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $data = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'age' => $this->input->post('age'),
                    'gender' => $this->input->post('gender'),
                    'currentFieldOfWork' => $this->input->post('currentFieldOfWork')
                );

                $isAdded = $this->app->addContent($this->tbl, $data);
                if ($isAdded) {
                    $this->session->set_flashdata('success', "Recertification added successfully");
                    redirect("admin/recertifications");
                } else {
                    $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        };
//        echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/addRecertification');
        $this->load->view('admin/template/footer');
    }

    function recertificationDetail($id){
        $conditions = array('table'=>$this->tbl,'where'=>array($this->tbl.'.id'=>$id),'fields'=>'firstName,lastName,age,gender,currentFieldOfWork');
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/recertifications');
        }
        $data['data'] = $content[0];
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/recertificationDetail',$data);
        $this->load->view('admin/template/footer');
    }

    public function editRecertificaton($id){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('firstName', 'First Name', 'required');            
            $this->form_validation->set_rules('lastName', 'Last Name', 'required');
            $this->form_validation->set_rules('age', 'Age', 'required');            
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('currentFieldOfWork', 'Current Field of Work', 'required');           
            
            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $data = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'age' => $this->input->post('age'),
                    'gender' => $this->input->post('gender'),
                    'currentFieldOfWork' => $this->input->post('currentFieldOfWork')
                );

                $isUpdated = $this->app->updateRecord($this->tbl, array('id'=>$id),$data);
                if ($isUpdated) {
                    $this->session->set_flashdata('success', "Recertification updated successfully");
                    redirect("admin/recertifications");
                } else {
                    $this->session->set_flashdata('success', 'Nothing Changed');
                    redirect("admin/recertifications");
                }
            }
        }

        $conditions = array('table'=>$this->tbl,'where'=>array($this->tbl.'.id'=>$id),'fields'=>'id,firstName,lastName,age,gender,currentFieldOfWork');
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/recertifications');
        }

        $data['content'] = $content[0];
        $enrolledPro = array();
        $this->load->view('admin/template/header');
        $this->load->view('admin/editRecertification',$data);
        $this->load->view('admin/template/footer');
    }

    function deleteRecertification($id){
        $this->app->delete($this->tbl,'id',$id);
        $this->session->set_flashdata('success', "Recertification deleted successfully");
        redirect("admin/recertifications");
    }
}