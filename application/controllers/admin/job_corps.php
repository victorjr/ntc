<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Job_corps extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();
    var $tbl = 'job_corps';
    var $fields = 'job_corps.id,job_corps.firstName,job_corps.lastName,job_corps.dateApply,trades.title as tradeName,job_corps.created';

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $limit = 20;
        $join = array('joinTbl'=>'trades', 'on'=>$this->tbl.'.trade = trades.id','type'=>'left');
        $conditions = array(
            'table'=>$this->tbl,
            'order'=>$this->tbl.".id DESC",
            'join'=>array($join)
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "firstName like '%".$q."%' OR lastName like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/job_corps/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/job_corps',$data);
        $this->load->view('admin/template/footer');

    }

    public function waiting_list()
    {
        $limit = 20;
        $join = array('joinTbl'=>'trades', 'on'=>$this->tbl.'.trade = trades.id','type'=>'left');
        $conditions = array(
            'table'=>$this->tbl,
            'where'=>Array ($this->tbl.'.status' => "Waiting List"),
            'order'=>$this->tbl.".id DESC",
            'join'=>array($join)
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "firstName like '%".$q."%' OR lastName like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/job_corps/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/waiting_list',$data);
        $this->load->view('admin/template/footer');

    }    

    public function intake()
    {
        $limit = 20;
        $join = array('joinTbl'=>'trades', 'on'=>$this->tbl.'.trade = trades.id','type'=>'left');
        $conditions = array(
            'table'=>$this->tbl,
            'where'=>Array ($this->tbl.'.status' => "Intake"),
            'order'=>$this->tbl.".id DESC",
            'join'=>array($join)
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "firstName like '%".$q."%' OR lastName like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/job_corps/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/intake',$data);
        $this->load->view('admin/template/footer');

    }    


public function graduate()
    {
        $limit = 20;
        $join = array('joinTbl'=>'trades', 'on'=>$this->tbl.'.trade = trades.id','type'=>'left');
        $conditions = array(
            'table'=>$this->tbl,
            'where'=>Array ($this->tbl.'.status' => "Graduate"),
            'order'=>$this->tbl.".id DESC",
            'join'=>array($join)
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "firstName like '%".$q."%' OR lastName like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/job_corps/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/graduate',$data);
        $this->load->view('admin/template/footer');

    }        

    function jobCorpDetail($id){
        $join = array('joinTbl'=>'trades', 'on'=>$this->tbl.'.trade = trades.id','type'=>'left');
        $followJoin = array('joinTbl'=>'follow_up', 'on'=>$this->tbl.'.id = follow_up.jobCorpId','type'=>'left');
        $conditions = array('table'=>$this->tbl,'where'=>array($this->tbl.'.id'=>$id),'fields'=>$this->tbl.'.*,trades.title as tradeTitle,follow_up.enrolmentId,follow_up.id as followUpId,follow_up.jobCorpId,follow_up.followUpDate,follow_up.sentReminder','join'=>array($join,$followJoin));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/job_corps');
        }
        $data['data'] = $content[0];
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/jobCorpDetail',$data);
        $this->load->view('admin/template/footer');
    }

    public function addJobCorp(){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            // echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('ssn', 'SSN', 'required');
            $this->form_validation->set_rules('firstName', 'First Name', 'required');            
            $this->form_validation->set_rules('lastName', 'Last Name', 'required');            
            $this->form_validation->set_rules('gender', 'Gender', 'required');            
            $this->form_validation->set_rules('dob', 'DOB', 'required');            
            $this->form_validation->set_rules('trade', 'Trade Area', 'required');            
            $this->form_validation->set_rules('dateApply', 'Date Applied', 'required');            
            $this->form_validation->set_rules('intakeDate', 'Intake Date', 'required');            
            $this->form_validation->set_rules('status', 'Status', 'required');            
            $this->form_validation->set_rules('campus', 'Campus', 'required');            
            
            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $data = array(
                    'ssn' => $this->input->post('ssn'),
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'gender' => $this->input->post('gender'),
                    'dob' => $this->input->post('dob'),
                    'trade' => $this->input->post('trade'),
                    'dateApply' => $this->input->post('dateApply'),
                    'intakeDate' => $this->input->post('intakeDate'),
                    'exitStatus' => $this->input->post('exitStatus'),
                    'remarks' => $this->input->post('remarks'),
                    'terminated' => $this->input->post('terminated'),
                    'status' => $this->input->post('status'),
                    'campus' => $this->input->post('campus'),
                    'created'=>date('Y-m-d H:i:s')
                );
                if ($this->input->post('exitDate') != null) {
                    $data['exitDate'] = $this->input->post('exitDate');
                }
                $isAdded = $this->app->addContent($this->tbl, $data);
                if ($isAdded && ($this->input->post('exitDate') != null)) {
                    $lId = $this->app->lastId();
                    // add follow up
                    $followUp = array(
                        // 'enrolmentId' => $this->input->post('enrolment'),
                        'jobCorpId' => $lId,
                        'followUpDate' => $this->input->post('exitDate'),
                        'sentReminder' => $this->input->post('sentReminder')
                    );
                    $isAdded = $this->app->addContent('follow_up', $followUp);
                    $this->session->set_flashdata('success', "Job Corp added successfully");
                    redirect("admin/job_corps");
                } else if ($isAdded) {
                    $this->session->set_flashdata('success', "Job Corp added successfully");
                    redirect("admin/job_corps");
                } else {
                    $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }

        $trades = $this->app->getData(array('table'=>'trades'));
        $data['tradeList'] = $trades;
        $data['exitStatuses'] = array(
            0 => 'Employment',
            1 => 'Advanced Training',
            2 => 'Go Back to School',
            3 => 'Military' 
        );
//        echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/addJobCorp',$data);
        $this->load->view('admin/template/footer');
    }

    function getTrainee(){
        $id = $this->input->post('id');
        $user = $this->app->getData(array('table'=>'trainees','where'=>array('ssn'=>$id)));
        $data['user'] = $user;
        // get enrolled programs
        $content = array();
        if(isset($user[0]['id'])) {
            $join = array('joinTbl' => 'training_programs', 'on' => 'enrollments.programId = training_programs.id', 'type' => 'left');
            $conditions = array('table' => 'enrollments', 'where' => array('enrollments.traineeId' => $user[0]['id']), 'fields' => 'enrollments.id,enrollments.programId,training_programs.title as programTitle', 'join' => array($join));
            $content = $this->app->getData($conditions);
        }
        $data['enrolledPro'] = $content;
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/getTrainee',$data);
    }

    public function editJobCorp($id){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('ssn', 'SSN', 'trim|required');
            $this->form_validation->set_rules('enrolment', 'Enrolment', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //echo '<pre>';print_r($_POST);exit;
                $data = array(
                    'ssn' => $this->input->post('ssn'),
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'gender' => $this->input->post('gender'),
                    'dob' => $this->input->post('dob'),
                    'trade' => $this->input->post('trade'),
                    'dateApply' => $this->input->post('dateApply'),
                    'intakeDate' => $this->input->post('intakeDate'),
                    'exitDate' => $this->input->post('exitDate'),
                    'remarks' => $this->input->post('remarks'),
                    'terminated' => $this->input->post('terminated'),
                    'status' => $this->input->post('status'),
                    'center' => $this->input->post('center'),
                    'enrollment_date' => $this->input->post('enrollment_date')
                );


                $isUpdated = $this->app->updateRecord($this->tbl, array('id'=>$id),$data);
                if ($isUpdated) {
                    $followUp = array(
                        'enrolmentId'=>$this->input->post('enrolment'),
                        'jobCorpId'=>$id,
                        'followUpDate'=>$this->input->post('exitDate'),
                        'sentReminder'=>$this->input->post('sentReminder')
                    );
                    $this->app->updateRecord('follow_up', array('id'=>$this->input->post('followUpId')),$followUp);
                    $this->session->set_flashdata('success', "Job Corp updated successfully");
                    redirect("admin/job_corps");
                } else {
                    $this->session->set_flashdata('success', 'Nothing Changed');
                    redirect("admin/job_corps");
                }
            }
        }

        $join = array('joinTbl' => 'follow_up', 'on' => 'job_corps.id = follow_up.jobCorpId', 'type' => 'left');
        $traineeJoin = array('joinTbl' => 'trainees', 'on' => 'job_corps.ssn = trainees.ssn', 'type' => 'left');
        $conditions = array('table'=>$this->tbl,'fields'=>$this->tbl.'.*,trainees.ssn,trainees.firstName,trainees.lastName,trainees.dob,trainees.gender,follow_up.enrolmentId,follow_up.id as followUpId,follow_up.jobCorpId,follow_up.followUpDate,follow_up.sentReminder','where'=>array($this->tbl.'.id'=>$id),'join'=>array($join,$traineeJoin));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/job_corps');
        }

        $data['content'] = $content[0];
        $trades = $this->app->getData(array('table'=>'trades'));
        $data['tradeList'] = $trades;
        $users = $this->app->getData(array('table'=>'trainees'));
        $data['usersList'] = $users;
        $enrolledPro = array();
        if(isset($users[0]['id'])) {
            $join = array('joinTbl' => 'training_programs', 'on' => 'enrollments.programId = training_programs.id', 'type' => 'left');
            $conditions = array('table' => 'enrollments', 'where' => array('enrollments.traineeId' => $users[0]['id']), 'fields' => 'enrollments.id,enrollments.programId,training_programs.title as programTitle', 'join' => array($join));
            $enrolledPro = $this->app->getData($conditions);
        }
        $data['enrolledPro'] = $enrolledPro;
        //echo '<pre>';       print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/editJobCorp',$data);
        $this->load->view('admin/template/footer');
    }

    function deleteJobCorp($id){
        $this->app->delete($this->tbl,'id',$id);
        $this->app->delete('follow_up','jobCorpId',$id);
        $this->session->set_flashdata('success', "Job Corp deleted successfully");
        redirect("admin/job_corps");

    }

}