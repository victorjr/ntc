<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Categories extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();
    var $tbl = 'categories';
    var $fields = 'id,title,created';

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $limit = 20;
        $conditions = array(
            'table'=>$this->tbl,
            'order'=>"id DESC"
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "title like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/categories/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/categories',$data);
        $this->load->view('admin/template/footer');

    }

    public function addCategory(){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Name', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //$file = $this->imageUpload('file');
                $data = array(
                    'title' => $this->input->post('title')
                );

                $isAdded = $this->app->addContent($this->tbl, $data);
                if ($isAdded) {
                    $this->session->set_flashdata('success', "Category added successfully");
                    redirect("admin/categories");
                } else {
                    $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/addCategory');
        $this->load->view('admin/template/footer');
    }

    public function editCategory($id){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Name', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
               // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //$file = $this->imageUpload('file');
                $data = array(
                    'title' => $this->input->post('title')
                );

                $isUpdated = $this->app->updateRecord($this->tbl, array('id'=>$id),$data);
                if ($isUpdated) {
                    $this->session->set_flashdata('success', "Category updated successfully");
                    redirect("admin/categories");
                } else {
                    $this->session->set_flashdata('success', 'Nothing Changed');
                    redirect("admin/categories");
                }
            }
        }

        $conditions = array('table'=>$this->tbl,'where'=>array('id'=>$id));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/categories');
        }

        $data['content'] = $content[0];
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/editCategory',$data);
        $this->load->view('admin/template/footer');
    }

    function deleteCategory($id){
        $this->app->delete($this->tbl,'id',$id);
        $this->session->set_flashdata('success', "Category deleted successfully");
        redirect("admin/categories");

    }

}