<div class="content">
    <!-- CKEditor default -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Training Program Detail</h5>
            <a class="pull-right" href="<?php echo base_url('admin/enrollments/addEnrollment/'.$id)?>">
                <button class="btn btn-success" type="button">Enroll Trainee</button>
            </a>
        </div>
        <div class="panel-body">
            <table class="table table-lg">
                <tbody>
                <tr>
                    <th colspan="3" class="active">Detail</th>
                </tr>
                <tr>
                    <td>Training Program:</td>
                    <td><?php echo $data['title'];?></td>
                </tr>
                <tr>
                    <td>Trainee:</td>
                    <td><?php echo $data['firstName'].' '.$data['lastName'];?></td>
                </tr>

                <tr>
                    <td>Applied Date:</td>
                    <td><?php echo date('M d,Y',strtotime($data['appliedDate']));?></td>
                </tr>
                <tr>
                    <td>Accepted:</td>
                    <td><?php if($data['accepted']) echo 'Yes';else echo 'No';?></td>
                </tr>
                <tr>
                    <td>Completed:</td>
                    <td><?php if($data['completed']) echo 'Yes';else echo 'No';?></td>
                </tr>
                <tr>
                    <td>Remarks:</td>
                    <td><?php echo $data['remarks'];?></td>
                </tr>
                <tr>
                    <td>Past Programs:</td>
                    <td><?php echo $data['pastProgram'];?></td>
                </tr>
                <tr>
                    <td>Training Type:</td>
                    <td><?php echo $data['trainingType'];?></td>
                </tr>
                <tr>
                    <td>Is Enrolled Before:</td>
                    <td><?php if($data['isEnrolledBefore']) echo 'Yes';else echo 'No';?></td>
                </tr>
                <tr>
                    <td>Year Of Same Training:</td>
                    <td><?php echo $data['yearOfSameTraining'];?></td>
                </tr>
                <tr>
                    <td>Day Length Of Same Training:</td>
                    <td><?php echo round($data['dayLengthOfSameTraining'],1);?></td>
                </tr>
                <tr>
                    <td>Will Update Ntc On Plans:</td>
                    <td><?php if($data['willUpdateNtcOnPlans']) echo 'Yes';else echo 'No';?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>