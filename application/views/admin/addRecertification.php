<!-- Content area -->
<div class="content">



<!-- CKEditor default -->

<div class="panel panel-flat padding-form-tran">

    <div class="panel-heading">
        <h5 class="panel-title">Add Recertification</h5>
    </div>

    <div class="panel-body">

        <form method="post" id="programAdd" class="form-horizontal" action="<?php echo(base_url('admin/recertifications/addRecertification')); ?>" enctype="multipart/form-data">

            <fieldset class="content-group">
                <div id="dynamicC">
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">First Name: </label>
                        <div class="">
                            <div class="error"><?php echo form_error('firstName'); ?></div>
                            <input type="text" required="" name="firstName" value="<?php echo set_value('firstName'); ?>" class="form-control">
                        </div>
                    </div>
                </div>    
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">Last Name: </label>
                        <div class="">
                            <div class="error"> <?php echo form_error('lastName'); ?></div>
                            <input type="text" required="" name="lastName" value="<?php echo set_value('lastName'); ?>" class="form-control">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-12 col-sm-12 padding-none">  
                <div class="form-group">
                    <label class="control-label col-lg-2">Gender: </label>
                    <div class="col-lg-10">
                        <div class="col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label col-lg-2">Male: </label>
                                <input type="radio" name="gender" value="male" class="form-control" style="position: absolute; height: 20px;top:4px;margin-left: 50px;">
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-lg-4">Female: </label>
                                <input type="radio" name="gender" value="female" class="form-control" style="position: absolute; height: 20px;top:4px;margin-left: 60px;">
                            </div>
                        </div>
                    </div>
                </div>
                </div> 
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">Age: </label>
                        <div class="">
                            <div class="error"> <?php echo form_error('age'); ?></div>
                            <input type="number" required="" name="age" value="<?php echo set_value('age'); ?>" class="form-control">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">Current Field of Work: </label>
                        <div class="">
                            <div class="error"> <?php echo form_error('currentFieldOfWork'); ?></div>
                            <input type="text" required="" name="currentFieldOfWork" value="<?php echo set_value('currentFieldOfWork'); ?>" class="form-control">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-12 col-sm-12 padding-none">    
                    <div class="form-group">
                        <div class="col-lg-3">
                            <button type="submit" class="btn bg-teal-400">Submit<i class="icon-arrow-right14 position-right"></i></button>
                            <a href="<?php echo base_url('admin/recertifications')?>">
                                <button type="button" class="btn bg-teal-400">Cancel<i class="icon-arrow-right14 position-right"></i></button>
                            </a>
                        </div>
                    </div>
                </div>
            </fieldset>

        </form>

    </div>

</div>

<!-- /CKEditor default -->