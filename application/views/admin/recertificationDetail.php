<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title">Recertification Detail</h5>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                <tr>

                    <th colspan="3" class="active title">Detail</th>

                </tr>

                <tr>

                    <td class="background">First Name:</td>

                    <td><?php echo $data['firstName'];?></td>

                </tr>

                <tr>

                    <td class="background">Last Name:</td>

                    <td><?php echo $data['lastName'];?></td>

                </tr>

                <tr>

                    <td class="background">Gender:</td>

                    <td><?php echo $data['gender'];?></td>

                </tr>

                <tr>

                    <td class="background">Age:</td>

                    <td><?php echo $data['age'];?></td>

                </tr>

                <tr>

                    <td class="background">Current Field of Work:</td>

                    <td><?php echo $data['currentFieldOfWork'];?></td>

                </tr>

                </tbody>

            </table>

        </div>

    </div>

</div>