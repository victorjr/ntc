<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title">C&EC Detail</h5>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                <tr>

                    <th colspan="3" class="active title">Detail</th>

                </tr>

                <tr>

                    <td class="background">Social Security Number:</td>

                    <td><?php echo $data['ssn'];?></td>

                </tr>

                <tr>

                    <td class="background">First Name:</td>

                    <td><?php echo $data['firstName'];?></td>

                </tr>

                <tr>

                    <td class="background">Last Name:</td>

                    <td><?php echo $data['lastName'];?></td>

                </tr>

                <tr>

                    <td class="background">Remarks:</td>

                    <td><?php echo $data['remarks'];?></td>

                </tr>

                <tr>

                    <td class="background">File Uploaded:</td>

                    <td><a href="<?php echo base_url('uploads/'.$data['fileLink']) ?>"><?php echo $data['fileLink'];?></a></td>

                </tr>

                </tbody>

            </table>

        </div>

    </div>

</div>