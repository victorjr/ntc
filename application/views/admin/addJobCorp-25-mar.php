<!-- Content area -->
<div class="content">



<!-- CKEditor default -->

<div class="panel panel-flat padding-form-tran">

    <div class="panel-heading">
        <h5 class="panel-title">Add Job Corp Detail</h5>
    </div>



    <div class="panel-body">

        <form method="post" id="programAdd" class="form-horizontal" action="<?php echo(base_url('admin/job_corps/addJobCorp')); ?>" enctype="multipart/form-data">

            <fieldset class="content-group">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group" id="ssnC">
                        <label class="control-label">Social Security Number:</label>
                        <div class="">
                            <select id="ssn" name="ssn" required="" class="form-control">
                                <option value="">Select social security number</option>
                                <?php foreach ($usersList as $v) {?>
                                    <option value="<?php echo $v['ssn'];?>"><?php echo $v['ssn'];?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>    
                <div id="dynamicC">
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">First Name: </label>
                        <div class="">
                            <div class="error"><?php echo form_error('firstName'); ?></div>
                            <input type="text" readonly required="" name="firstName" value="<?php echo set_value('firstName'); ?>" class="form-control">
                        </div>
                    </div>
                </div>    
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">Last Name: </label>
                        <div class="">
                            <div class="error"> <?php echo form_error('lastName'); ?></div>
                            <input type="text" readonly required="" name="lastName" value="<?php echo set_value('lastName'); ?>" class="form-control">
                        </div>
                    </div>
                </div>    
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group" id="ssnC">
                        <label class="control-label">Select Enrolment:</label>
                        <div class="">
                            <select id="enrolment" name="enrolment" required="" class="form-control"></select>
                        </div>
                    </div>
                </div>   
                <div class="col-lg-12 col-sm-12 padding-none">  
                    <div class="form-group">
                        <label class="control-label col-lg-2">Gender: </label>
                        <div class="col-lg-10">
                            <div class="col-lg-6">
                                <div class="col-lg-6">
                                    <label class="control-label col-lg-2">Male: </label>
                                    <input disabled type="radio" name="gender" value="male" class="form-control" style="position: absolute; height: 20px;top:4px;margin-left: 50px;">
                                </div>
                                <div class="col-lg-6">
                                    <label class="control-label col-lg-4">Female: </label>
                                    <input disabled type="radio" name="gender" value="female" class="form-control" style="position: absolute; height: 20px;top:4px;margin-left: 60px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6 col-sm-6">   
                    <div class="form-group">
                        <label class="control-label"> DOB: </label>
                        <div class="">
                            <?php echo form_error('dob'); ?>
                            <input readonly="" type="text" name="dob" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control">
                        </div>
                    </div>
                </div>    
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group" id="tradeC">
                        <label class="control-label ">Trade:</label>
                        <div class="">
                            <select id="trade" name="trade" class="form-control">
                                <?php foreach ($tradeList as $v) {?>
                                    <option value="<?php echo $v['id'];?>"><?php echo $v['title'];?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label"> Applied Date: </label>
                        <div class="">
                            <?php echo form_error('dateApply'); ?>
                            <input type="text" name="dateApply" id="dateApply" value="<?php echo set_value('dateApply'); ?>" class="form-control pickadate-max-limits">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6 col-sm-6">   
                    <div class="form-group">
                        <label class="control-label"> Intake Date: </label>
                        <div class="">
                            <?php echo form_error('intakeDate'); ?>
                            <input type="text" name="intakeDate" id="intakeDate" value="<?php echo set_value('intakeDate'); ?>" class="form-control pickadate-limits">
                        </div>
                    </div>
                </div>    
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <label class="control-label"> 	Exit Date: </label>
                        <div class="">
                            <?php echo form_error('exitDate'); ?>
                            <input type="text" name="exitDate" id="exitDate" value="<?php echo set_value('exitDate'); ?>" class="form-control pickadate-limits">
                        </div>
                    </div>
                </div>    
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group"  style="display:none;">
                        <label class="control-label col-lg-2"> 	Follow Up Date: </label>
                        <div class="col-lg-6">
                            <?php echo form_error('followUpDate'); ?>
                            <input type="text" name="followUpDate" id="followUpDate" value="<?php echo set_value('followUpDate'); ?>" class="form-control pickadate-limits">
                        </div>
                    </div>
                </div>    
                <div class="form-group" style="display:none;">
                    <label class="control-label col-lg-2">	Sent Reminder: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('sentReminder'); ?></div>
                        <input type="checkbox" name="sentReminder" value="1" class="form-control">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Terminated: </label>
                        <div class="">
                            <div class="error"> <?php echo form_error('terminated'); ?></div>
                            <input type="checkbox" name="terminated" value="1" class="" style="height: 28px;">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">        
                    <div class="form-group">
                        <label class="control-label">Status: </label>
                        <div class="">
                            <select name="status" class="form-control">
                                <option value="Waiting List">
                                    Waiting List
                                </option>
                                <option value="Intake">
                                    Intake
                                </option>
                                <option value="Graduate">
                                    Graduate
                                </option>
                            </select>
                        </div>
                    </div>
                </div>   
                <div class="col-lg-6 col-sm-6">  
                    <div class="form-group">
                        <label class="control-label">Center: </label>
                        <div class="">
                            <select name="center" class="form-control">
                                <option value="Honolulu">
                                    Honolulu
                                </option>
                                <option value="Maui">
                                    Maui
                                </option>
                                <option value="Arkansa">
                                    Arkansa
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">     
                    <div class="form-group">
                        <label class="control-label"> Enrollment Year: </label>
                        <div class="">
                            <?php echo form_error('enrollment_date'); ?>
                            <input type="text" name="enrollment_date" id="enrollment_date" value="<?php echo set_value('enrollment_date'); ?>" class="form-control pickadate-limits">
                        </div>
                    </div>
                </div>   
                <div class="col-lg-6 col-sm-6">  
                    <div class="form-group">
                        <label class="control-label">  Remarks: </label>
                        <div class="">
                            <?php echo form_error('remarks'); ?>
                            <textarea type="text" name="remarks" id="remarks" class="form-control"><?php echo set_value('remarks'); ?></textarea>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-12 col-sm-12 padding-none">    
                    <div class="form-group">
                        <div class="col-lg-3">
                            <button type="submit" class="btn bg-teal-400">Submit<i class="icon-arrow-right14 position-right"></i></button>
                            <a href="<?php echo base_url('admin/job_corps')?>">
                                <button type="button" class="btn bg-teal-400">Cancel<i class="icon-arrow-right14 position-right"></i></button>
                            </a>
                        </div>
                    </div>
                </div>
            </fieldset>

        </form>

    </div>

</div>
<script>
    $(document).ready(function(){
        $('#ssn').on('change', function() {
            $("#loaderImg").show();
            var elm = $('option:selected', this);
            var id = elm.val();
            var url = '<?php  echo base_url('admin/job_corps/getTrainee');?>';
            $.ajax(
                {
                    url: url,
                    type: "POST",
                    dataType: "text",
                    data: {id:id},
                    success: function (data) {
                        if (data != '') {
                            //alert(JSON.stringify(data));
                            $('#dynamicC').html(data);
                        }
                        $("#loaderImg").hide();


                        //data: return data from server
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#loaderImg").hide();
                        alert("Something went wrong... Please try again")
                        //if fails
                    }
                });
        });
    });
</script>

<!-- /CKEditor default -->