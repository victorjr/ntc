<!-- Content area -->
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="content">
		<!-- CKEditor default -->
		<div class="panel panel-flat padding-form-tran">
			<div class="panel-heading">
				<h5 class="panel-title">Add Training Program Detail</h5>
			</div>
			<div class="panel-body">
				<form action=" <?php echo(base_url('admin/programs/addProgram')); ?>" class="form-horizontal" enctype="multipart/form-data" id="programAdd" method="post" name="programAdd">
					<fieldset class="content-group">
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Title:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('title'); ?>
									</div><input class="form-control" name="title" required="" type="text" value="<?php echo set_value('title'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Contact:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('contact'); ?>
									</div><input class="form-control" name="contact" required="" type="text" value="<?php echo set_value('contact'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Organization:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('organization'); ?>
									</div><input class="form-control" name="organization" type="text" value="<?php echo set_value('organization'); ?>">
								</div>
							</div>
						</div>
						<div class="">
							<div class="col-lg-2 col-sm-2 checkbox-padding">
								<div class="form-group">
									<label class="control-label">Established:</label>
									<div class="">
										<div class="error">
											<?php echo form_error('established'); ?>
										</div><input class="form-control" name="established" style="height: 15px;" type="checkbox" value="1">
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-sm-2 checkbox-padding">
								<div class="form-group">
									<label class="control-label">Academic Credit:</label>
									<div class="">
										<div class="error">
											<?php echo form_error('academicCredit'); ?>
										</div><input class="form-control" name="academicCredit" style="height: 15px;" type="checkbox" value="1">
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-sm-2 checkbox-padding">
								<div class="form-group">
									<label class="control-label">Certification:</label>
									<div class="">
										<div class="error">
											<?php echo form_error('certification'); ?>
										</div><input class="form-control" name="certification" style="height: 15px;" type="checkbox" value="1">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group" id="categoryC">
								<label class="control-label">Focus Training Area:</label>
								<div class="">
									<select class="form-control" id="category" name="category">
										<?php foreach ($catList as $v) {?>
										<option value="<?php echo $v['id'];?>">
											<?php echo $v['title'];?>
										</option><?php }?>
									</select>
								</div>
							</div>
						</div>
            <div class="col-lg-6 col-sm-6">
							<div class="form-group" id="trainingType">
								<label class="control-label">Training Type:</label>
								<div class="">
									<select class="form-control" id="trainingType" name="trainingType">
										<?php foreach ($trainingType as $v) {?>
										<option value="<?php echo $v['id'];?>">
											<?php echo $v['title'];?>
										</option><?php }?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Location:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('location'); ?>
									</div><input class="form-control" name="location" type="text" value="<?php echo set_value('location'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Start Date:</label>
								<div class="">
									<?php echo form_error('startDate'); ?><input class="form-control pickadate-min-limits" id="startDate" name="startDate" type="text" value="<?php echo set_value('startDate'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">End Date:</label>
								<div class="">
									<?php echo form_error('endDate'); ?><input class="form-control pickadate-min-limits" id="endDate" name="endDate" type="text" value="<?php echo set_value('endDate'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Duration:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('duration'); ?>
									</div><input class="form-control" name="duration" type="text" value="<?php echo set_value('duration'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Amount Requested:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('amountRequested'); ?>
									</div><input class="form-control" min="1" name="amountRequested" step="0" type="number" value="<?php echo set_value('amountRequested'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Amount Approved:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('amountApproved'); ?>
									</div><input class="form-control" min="1" name="amountApproved" step="0" type="number" value="<?php echo set_value('amountApproved'); ?>">
								</div>
							</div>
						</div>
            <div class="col-lg-6 col-sm-6">
							<div class="form-group" id="sourceFund">
								<label class="control-label">Source of Fund:</label>
								<div class="">
									<select class="form-control" id="sourceFund" name="sourceFund">
										<option>NRW Fund</option>
                    <option>General Fund</option>
                    <option>SEG Fund</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">SEG Fund:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('segFund'); ?>
									</div><input class="form-control" min="1" name="segFund" step="0" type="number" value="<?php echo set_value('segFund'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-sm-12">
							<div class="form-group">
								<label class="control-label">NRW Fund:</label>
								<div class="">
									<div class="error">
										<?php echo form_error('nrwFund'); ?>
									</div><input class="form-control" min="1" name="nrwFund" step="0" type="number" value="<?php echo set_value('nrwFund'); ?>">
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-sm-12 padding-none">
							<div class="form-group">
								<div class="col-lg-3">
									<button class="btn bg-teal-400" type="submit">Submit<i class="icon-arrow-right14 position-right"></i></button> <a href=" <?php echo base_url('admin/programs')?>"><button class="btn bg-teal-400" type="button">Cancel<i class="icon-arrow-right14 position-right"></i></button></a>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
		<script>
		  $(document).ready(function(){        $("#programAdd").submit(function(e){            //e.preventDefault();            if($('#endDate').val() < $('#startDate').val()){                alert("end date should be greater or equal to start date");                return false;            }        });        /*$('#states').on('change', function() {            $("#loaderImg").show();            var elm = $('option:selected', this);            var id = elm.val();            var url = '<?php // echo base_url('admin/users/getCities');?>';            $.ajax(                {                    url: url,                    type: "POST",                    dataType: "text",                    data: {id:id},                    success: function (data) {                        if (data != '') {                            //alert(JSON.stringify(data));                            $("#loaderImg").hide();                            $('#cities').html(data);                        }                        //data: return data from server                    },                    error: function (jqXHR, textStatus, errorThrown) {                        $("#loaderImg").hide();                        alert("Something went wrong... Please try again")                        //if fails                    }                });        });*/    });
		</script><!-- /CKEditor default -->
	</div>
</body>
</html>