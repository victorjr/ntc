<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title">Trainer Details</h5>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                    <tr>

                        <th colspan="3" class="active title">Detail</th>

                    </tr>

                    <tr>

                        <td class="background">First name:</td>

                        <td><?php echo $data['content']['firstName'];?></td>

                    </tr>

                    <tr>

                        <td class="background">Last Name:</td>

                        <td><?php echo $data['content']['lastName'];?></td>

                    </tr>

                    <tr>

                        <td class="background">Training Programs:</td>

                        <td>

                            <?php 
                                foreach ( $data['trainings'] as $training_program ){
                                    foreach ( $data['trainersPrograms'] AS $t_p ){
                                        if ( $t_p['programId'] == $training_program['id'] ) {
                                            echo $training_program['title'] . '<br>';
                                        }
                                    }
                                }
                                // echo $data['content']['lastName'];
                            ?>
                                
                        </td>

                    </tr>

                    <tr>

                        <td class="background">Qualifications:</td>

                        <td>

                            <?php 
                                foreach ( $data['qualification'] as $qualification ){
                                    foreach ( $data['qualifications'] AS $qs ){
                                        if ( $qs['qualificationId'] == $qualification['id'] ) {
                                            echo $qualification['title'] . '<br>';
                                        }
                                    }
                                }
                                // echo $data['content']['lastName'];
                            ?>
                                
                        </td>

                    </tr>


                </tbody>

            </table>

        </div>

    </div>

</div>