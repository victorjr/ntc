<!-- Content area -->
<div class="content">



<!-- CKEditor default -->

<div class="panel panel-flat padding-form-tran">

    <div class="panel-heading">
        <h5 class="panel-title">Add C&EC</h5>
    </div>

    <div class="panel-body">

        <form method="post" id="programAdd" class="form-horizontal" action="<?php echo(base_url('admin/cec/addCec')); ?>" enctype="multipart/form-data">

            <fieldset class="content-group">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group" id="ssnC">
                        <label class="control-label">Social Security Number:</label>
                        <div class="">
                            <input id="ssn" name="ssn" required="" class="form-control" type="text" />
                        </div>
                    </div>
                </div>    
                <div id="dynamicC">
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">First Name: </label>
                        <div class="">
                            <div class="error"><?php echo form_error('firstName'); ?></div>
                            <input type="text" required="" name="firstName" value="<?php echo set_value('firstName'); ?>" class="form-control">
                        </div>
                    </div>
                </div>    
                <div class="col-lg-6 col-sm-6">    
                    <div class="form-group">
                        <label class="control-label">Last Name: </label>
                        <div class="">
                            <div class="error"> <?php echo form_error('lastName'); ?></div>
                            <input type="text" required="" name="lastName" value="<?php echo set_value('lastName'); ?>" class="form-control">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6 col-sm-6">  
                    <div class="form-group">
                        <label class="control-label">  Remarks: </label>
                        <div class="">
                            <?php echo form_error('remarks'); ?>
                            <textarea type="text" name="remarks" id="remarks" class="form-control"><?php echo set_value('remarks'); ?></textarea>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <label class="control-label"> File Upload: </label>
                        <input class="form-control" type="file" name="userfile" size="20" />
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 padding-none">    
                    <div class="form-group">
                        <div class="col-lg-3">
                            <button type="submit" class="btn bg-teal-400">Submit<i class="icon-arrow-right14 position-right"></i></button>
                            <a href="<?php echo base_url('admin/job_corps')?>">
                                <button type="button" class="btn bg-teal-400">Cancel<i class="icon-arrow-right14 position-right"></i></button>
                            </a>
                        </div>
                    </div>
                </div>
            </fieldset>

        </form>

    </div>

</div>

<!-- /CKEditor default -->