<!-- Content area -->
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="content">
		<!-- CKEditor default -->
		<div class="panel panel-flat padding-form-tran">
			<div class="panel-heading">
				<h5 class="panel-title">Edit CE&C Detail</h5>
			</div>
			<div class="panel-body">
				<form action="<?php echo(base_url('admin/cec/editCec/'.$content['id'])); ?>" class="form-horizontal" enctype="multipart/form-data" id="cecEdit" method="post" name="cecEdit">
					<fieldset class="content-group">
					<div class="col-lg-6 col-sm-6">
            <div class="form-group">
              <label class="control-label">First Name:</label>
              <div class="">
                <div class="error">
                  <?php echo form_error('ssn'); ?>
                </div><input class="form-control" name="ssn" required="" type="text" value="<?php echo $content['ssn']; ?>">
              </div>
            </div>
          </div>
						<div id="dynamicC">
							<div class="col-lg-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">First Name:</label>
									<div class="">
										<div class="error">
											<?php echo form_error('firstName'); ?>
										</div><input class="form-control" name="firstName" required="" type="text" value="<?php echo $content['firstName']; ?>">
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Last Name:</label>
									<div class="">
										<div class="error">
											<?php echo form_error('lastName'); ?>
										</div><input class="form-control" name="lastName" required="" type="text" value="<?php echo $content['lastName']; ?>">
									</div>
								</div>
							</div>
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Remarks:</label>
								<div class="">
									<?php echo form_error('remarks'); ?>
									<textarea class="form-control" id="remarks" name="remarks"><?php echo $content['remarks']; ?></textarea>
								</div>
							</div>
            </div>
            <div class="col-lg-6 col-sm-6">
              <div class="form-group">
                <label class="control-label"> File Upload (This will override the current file uploaded: <a href="<?php echo $content['fileLink']; ?>"><?php echo $content['fileLink']; ?></a>): </label>
                <input class="form-control" type="file" name="userfile" size="20" />
              </div>
            </div>
						<div class="col-lg-12 col-sm-12 padding-none">
							<div class="form-group">
								<div class="col-lg-3">
                  <button class="btn bg-teal-400" type="submit">Submit<i class="icon-arrow-right14 position-right"></i></button> <a href="<?php echo base_url('admin/job_corps')?>"><button class="btn bg-teal-400" type="button">Cancel<i class="icon-arrow-right14 position-right"></i></button></a>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>