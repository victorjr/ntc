<?php
/******************
 *
 * Text strings
 *
 */
    $lang['home'] = 'Inicio';
    $lang['about-us'] = 'Sobre Nosotros';
    $lang['who-we-are'] = 'Quienes somos';
    $lang['vision'] = 'Visión';
    $lang['mission'] = 'Misión';
    $lang['team'] = 'Equipo';
    $lang['projects&publications'] = 'Proyectos y Publicaciones';
    $lang['projects'] = 'Proyectos';
    $lang['financial-reports'] = 'Reportes Financieros';
    $lang['performance-reports'] = 'Reportes de Desempeño';
    $lang['be-prepared'] = 'Estar Preparado';
    $lang['training-videos'] = 'Videos de Entrenamiento';
    $lang['emergency-plans'] = 'Planes de Emergencia';
    $lang['training-courses'] = 'Cursos de Entrenamiento';
    $lang['warning&alerts'] = 'ALERTAS Y ADVERTENCIAS';
    $lang['make-appeal'] = 'Hacer Llamado';
    $lang['rehabilitation'] = 'Rehabilitación';
    $lang['get-involved'] = 'Involucrarse';
    $lang['be-a-volunteer'] = 'Ser un Voluntario';
    $lang['donation'] = 'Donación';
    $lang['disaster-information'] = 'Información de Desastres';

?>