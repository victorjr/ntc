<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title ">Trace Study Detail</h5>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                <tr>

                    <th colspan="3" class="active title">Detail</th>

                </tr>

                <tr>

                    <td class="background">First Name:</td>

                    <td><?php echo $data['firstName'];?></td>

                </tr>

                <tr>

                    <td class="background">Last Name:</td>

                    <td><?php echo $data['lastName'];?></td>

                </tr>

                <tr>

                    <td class="background">Graduated:</td>

                    <td><?php echo $data['graduated'];?></td>

                </tr>

                <tr>

                    <td class="background">Why did you not graduated:</td>

                    <td><?php echo ucfirst($data['why_did_you_not_graduated']);?></td>

                </tr>

                <tr>

                    <td class="background">What are you doing now:</td>

                    <td><?php echo ucfirst($data['what_are_you_doing_now']);?></td>

                </tr>

                <tr>

                    <td class="background">Working:</td>

                    <td><?php echo ucfirst($data['working']);?></td>

                </tr>

                <tr>

                    <td class="background">Why are you not working:</td>

                    <td><?php echo $data['why_are_you_not_working'];?></td>

                </tr>



                <tr>

                    <td class="background">Did employer offer job:</td>

                    <td><?php echo $data['did_employer_offer_job'];?></td>

                </tr>

                <tr>

                    <td class="background">No offer why:</td>

                    <td><?php echo $data['no_offer_why'];?></td>

                </tr>

                <tr>

                    <td class="background">Who is employer:</td>

                    <td><?php echo $data['who_is_employer'];?></td>

                </tr>

                <tr>

                    <td class="background">Self employed:</td>

                    <td><?php echo $data['self_employed'];?></td>

                </tr>

                <tr>

                    <td class="background">Work location:</td>

                    <td><?php echo $data['work_location'];?></td>

                </tr>

                <tr>

                    <td class="background">Job title:</td>

                    <td><?php echo $data['job_title'];?></td>

                </tr>

                <tr>

                    <td class="background">Using skills learning in training:</td>

                    <td><?php echo $data['using_skills_learning_in_training'];?></td>

                </tr>

                <tr>

                    <td class="background">Ever been promoted:</td>

                    <td><?php echo $data['ever_been_promoted'];?></td>

                </tr>

                <tr>

                    <td class="background">Earning more than before training:</td>

                    <td><?php echo $data['earning_more_than_before_training'];?></td>

                </tr>

                <tr>

                    <td class="background">Already or intend to enroll in other training:</td>

                    <td><?php echo $data['already_or_intend_to_enroll_in_other_training'];?></td>

                </tr>

                <tr>

                    <td class="background">If yes what type:</td>

                    <td><?php echo $data['if_yes_what_type'];?></td>

                </tr>

                <tr>

                    <td class="background">Conducted At:</td>

                    <td><?php echo date('M d, Y',strtotime($data['created']));?></td>

                </tr>

                </tbody>

            </table>

        </div>

    </div>

</div>