<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NTC</title> <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
    type="text/css">
    <link href="<?php echo(base_url()); ?>assets/admin/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo(base_url()); ?>assets/admin/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo(base_url()); ?>assets/admin/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo(base_url()); ?>assets/admin/css/minified/components.min.css" rel="stylesheet"
    type="text/css">
    <link href="<?php echo(base_url()); ?>assets/admin/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    type="text/css">
    <link href="<?php echo(base_url()); ?>assets/admin/css/style.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <script type="text/javascript"
    src="<?php echo(base_url()); ?>assets/admin/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo(base_url()); ?>assets/admin/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="<?php echo(base_url()); ?>assets/admin/js/pages/form_multiselect.js"></script>
    <script>
        $(document).ready(function () {
           $('#reports-sub-menu')
           .on('shown.bs.collapse', function() {
               $(this)
               .parent()
               .find("#report i:nth-of-type(2)")
               .removeClass("fa-angle-right")
               .addClass("fa-angle-down");
           })
           .on('hidden.bs.collapse', function() {
               $(this)
               .parent()
               .find("#report i:nth-of-type(2)")
               .removeClass("fa-angle-down")
               .addClass("fa-angle-right");
           });
           $('#Setting-sub-menu')
           .on('shown.bs.collapse', function() {
               $(this)
               .parent()
               .find("#setting i:nth-of-type(2)")
               .removeClass("fa-angle-right")
               .addClass("fa-angle-down");
           })
           .on('hidden.bs.collapse', function() {
               $(this)
               .parent()
               .find("#setting i:nth-of-type(2)")
               .removeClass("fa-angle-down")
               .addClass("fa-angle-right");
           });
           
           $('#job-crop-sub-menu')
           .on('shown.bs.collapse', function() {
               $(this)
               .parent()
               .find("#job_corps i:nth-of-type(2)")
               .removeClass("fa-angle-right")
               .addClass("fa-angle-down");
           })
           .on('hidden.bs.collapse', function() {
               $(this)
               .parent()
               .find("#job_corps i:nth-of-type(2)")
               .removeClass("fa-angle-down")
               .addClass("fa-angle-right");
           });
       });
   </script>
</head>
<body><?php $controller = $this->router->fetch_class();
    $method = $this->router->fetch_method();
    $last = $this->uri->total_segments();
    $last = $this->uri->segment($last); ?>    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>">
            <!--<img src="<?php /*echo(base_url()); */ ?>assets/admin/images/logo_light.png" alt="">-->            NTC
            Admin </a>
            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>
        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user"><a class="dropdown-toggle" data-toggle="dropdown"> <img
                    src="<?php echo(base_url()); ?>assets/admin/images/placeholder.jpg" alt=""> <span>John</span> <i
                    class="caret"></i> </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="<?php echo base_url('logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->        <!-- Page container -->
    <div class="page-container">        <!-- Page content -->
        <div class="page-content">        <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">        <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding" id="menuBoxX">
                            <ul class="navigation navigation-main navigation-accordion">        <!-- Main -->
                                <li <?php if ($controller == 'dashboard') echo "class='active'"; ?>><a
                                    href="<?php echo(base_url('admin/dashboard')); ?>"><i class="font-icon fa fa-home"></i> <span>Dashboard</span></a>
                                </li>
                                <!--<li <?php /*if($controller == 'projects') echo "class='active'";*/ ?>>            <a href="#"><i class="icon-home4"></i> <span>Projects and Publication</span></a>            <ul>                <li <?php /*if($controller == 'projects') echo "class='active'";*/ ?>><a href="<?php /*echo(base_url('admin/projects')); */ ?>">Projects</a></li>                <li <?php /*if($controller == 'reports') echo "class='active'";*/ ?>><a href="<?php /*echo(base_url('admin/reports')); */ ?>">Financial Reports</a></li>                <li <?php /*if($controller == 'performance') echo "class='active'";*/ ?>><a href="<?php /*echo(base_url('admin/performance')); */ ?>">Performance Reports</a></li>            </ul>        </li>-->
                                <!--<li <?php /*if($controller == 'employees') echo "class='active'";*/ ?>>            <a href="<?php /*echo(base_url('admin/employees')); */ ?>"><i class="icon-home4"></i> <span>Employees</span></a>        </li>        <li <?php /*if($controller == 'surveys') echo "class='active'";*/ ?>>            <a href="<?php /*echo(base_url('admin/surveys')); */ ?>"><i class="icon-home4"></i> <span>Surveys</span></a>        </li>-->


                                <li <?php if ($controller == 'internships' ) echo "class='active'"; ?>>
                                    <a
                                    href="<?php echo(base_url('admin/internships')); ?>">
                                        <i class="font-icon fa fa-keyboard-o"></i>
                                        <span>Internship</span>
                                    </a>
                                </li>

                                <li <?php if ($controller == 'recertifications' ) echo "class='active'"; ?>>
                                    <a
                                    href="<?php echo(base_url('admin/recertifications')); ?>">
                                        <i class="font-icon fa fa-id-card-o"></i>
                                        <span>Recertification</span>
                                    </a>
                                </li>

                                <li <?php if ($controller == 'carrer_and_employments' ) echo "class='active'"; ?>>
                                    <a
                                    href="<?php echo(base_url('admin/carrer_and_employments')); ?>">
                                        <i class="font-icon fa fa-renren"></i>
                                        <span>C&EC</span>
                                    </a>
                                </li>

                                <li <?php if ($controller == 'proposals' ) echo "class='active'"; ?>>
                                    <a
                                    href="<?php echo(base_url('admin/proposals')); ?>">
                                        <i class="font-icon fa fa-twitch"></i>
                                        <span>Proposals</span>
                                    </a>
                                </li>

                                <li <?php if ($controller == 'programs' || $controller == 'enrollments') echo "class='active'"; ?>><a
                                    href="<?php echo(base_url('admin/programs')); ?>"><i class="font-icon fa fa-transgender-alt"></i>
                                    <span>Training Programs</span></a></li>
                                <li <?php if ($controller == 'trainers') echo "class='active'"; ?>><a
                                        href="<?php echo(base_url('admin/trainers')); ?>"><i class="font-icon fa fa-users"></i> <span>Trainers</span></a>
                                    </li>
                                <li <?php if ($controller == 'trainees') echo "class='active'"; ?>><a
                                        href="<?php echo(base_url('admin/trainees')); ?>"><i class="font-icon fa fa-user"></i> <span>Trainees</span></a>
                                    </li>

                                <li
                                <?php 
                                            if($controller == 'job_corps' && $method == 'index') echo "class='active'"; 
                                        ?>              
                                >                
                                    <a href="javascript:;" id="job_corps"  data-toggle="collapse" data-target="#job-crop-sub-menu">
                                        <i class="font-icon icon-home4"></i> 
                                        <span>Job Corps</span>
                                        <i class="font-icon fa fa-angle-right"></i>
                                    </a>            
                                </li>

                                <div id="job-crop-sub-menu" class="collapse <?php 
                                        if($controller == 'job_corps')
                                            echo "in"; 
                                    ?> " >
                                        
                                    <li 
                                        <?php 
                                            if($controller == 'job_corps' && $method == 'waiting_list') echo "class='active'"; 
                                        ?> >                
                                            <a href="<?php echo(base_url('admin/job_corps/waiting_list'));  ?>">
                                                <i class="fa fa-server"></i> 
                                                <span>Waiting List</span>
                                            </a>            
                                    </li>

                                    <li 
                                        <?php 
                                            if($controller == 'job_corps' && $method == 'intake') echo "class='active'"; 
                                        ?> >                
                                            <a href="<?php echo(base_url('admin/job_corps/intake'));  ?>">
                                                <i class="fa fa-circle-o-notch"></i> 
                                                <span>Intake</span>
                                            </a>            
                                    </li>

                                    <li 
                                        <?php 
                                            if($controller == 'job_corps' && $method == 'graduate') echo "class='active'"; 
                                        ?> >                
                                            <a href="<?php echo(base_url('admin/job_corps/graduate'));  ?>">
                                                <i class="fa fa-graduation-cap"></i> 
                                                <span>Graduate</span>
                                            </a>            
                                    </li>
                                </div>


                                    <!--reports collapse start-->

                                <li <?php if($controller == 'citizenship') echo "class='active'"; ?>>
                                    <a href="javascript:;" id="report" data-toggle="collapse" data-target="#reports-sub-menu">
                                        <i class="fa  fa-bar-chart"></i> 
                                        <span>Reports</span> 
                                        <i class="font-icon fa fa-angle-right"></i>
                                    </a>            
                                </li>

                                    <div id="reports-sub-menu" class="collapse <?php if($controller == 'reports') echo "in"; ?>">
                                        <?php /*<li <?php if($controller == 'reports' && $method == 'index') echo "class='active'"; ?>>                <a href="<?php echo(base_url('admin/reports'));  ?>"><i class="font-icon fa fa-users"></i> <span>Training Programs Reports</span></a>            </li>
                                        <li <?php if($controller == 'reports' && $method == 'trainees_reports') echo "class='active'"; ?>>                <a href="<?php echo(base_url('admin/reports/trainees_reports'));  ?>"><i class="font-icon fa fa-user"></i> <span>Trainees Reports</span></a>            </li>
                                        <li <?php if($controller == 'reports' && $method == 'corps_reports') echo "class='active'"; ?>>                <a href="<?php echo(base_url('admin/reports/corps_reports'));  ?>"><i class="font-icon icon-home4"></i> <span>Job Corps Reports</span></a>            </li>
                                        */?>

                                        <li <?php if($controller == 'reports' && $method == 'program_summary') echo "class='active'"; ?>>
                                          <a href="<?php echo(base_url('admin/reports/program_summary'));  ?>">
                                            <i class="font-icon icon-home4"></i> 
                                            <span>Program Summary</span>
                                          </a>
                                        </li>

                                        <li <?php if($controller == 'reports' && $method == 'training_summary') echo "class='active'"; ?>>
                                          <a href="<?php echo(base_url('admin/reports/training_summary'));  ?>">
                                            <i class="font-icon icon-home4"></i> 
                                            <span>Training Summary</span>
                                          </a>
                                        </li>
                                    </div>
                                    <!--reports collapse emd-->

                                    <!--setting collapse start-->

                                    <li <?php if($controller == 'citizenship') echo "class='active'"; ?>>                <a href="javascript:;" id="setting" data-toggle="collapse" data-target="#Setting-sub-menu"><i class="fa  fa-cogs"></i> <span>Settings</span> <i class="font-icon fa fa-angle-right"></i></a>            </li>

                                    <div id="Setting-sub-menu" class="collapse <?php if($controller == 'trace' || $controller == 'trainingPlans' || $controller == 'atolls' || $controller == 'categories' || $controller == 'trades' || $controller == 'trainingTypes' || $controller == 'citizenship') echo "in"; ?>">
                                        <li <?php if($controller == 'citizenship') echo "class='active'"; ?>>                <a href="<?php echo(base_url('admin/citizenship'));  ?>"><i class="fa fa-id-card"></i> <span>Citizenship</span></a>            </li>


                                        <li <?php if($controller == 'atolls') echo "class='active'";?>>                <a href="<?php echo(base_url('admin/atolls'));  ?>"><i class="icon-home4"></i> <span>Atolls</span></a>            </li>
                                        <li <?php if($controller == 'categories') echo "class='active'";?>>
                                            <a href="<?php echo(base_url('admin/categories'));  ?>">
                                                <i class="fa fa-tags"></i> <span>Focus Training Area</span>
                                            </a>            
                                        </li>
                                        <li <?php if($controller == 'qualifications') echo "class='active'";?>>
                                            <a href="<?php echo(base_url('admin/qualifications'));  ?>">
                                                <i class="fa fa-tags"></i> <span>Qualifications</span>
                                            </a>            
                                        </li>
                                        <li <?php if($controller == 'trades') echo "class='active'";?>>                <a href="<?php echo(base_url('admin/trades'));  ?>"><i class="fa fa-line-chart"></i> <span>Trades</span></a>            </li>
                                        <li <?php if($controller == 'trainingTypes') echo "class='active'";?>>                <a href="<?php echo(base_url('admin/trainingTypes'));  ?>"><i class="fa fa-cubes"></i> <span>Training Types</span></a>            </li>
                                        <li <?php if($controller == 'trainingPlans') echo "class='active'";?>>                <a href="<?php echo(base_url('admin/trainingPlans'));  ?>"><i class="fa fa-bar-chart"></i> <span>Training Plans</span></a>            </li>
                                        <li <?php if($controller == 'trace') echo "class='active'"; ?>>                <a href="<?php echo(base_url('admin/trace'));  ?>"><i class="fa fa-id-card"></i> <span>Trace Study</span></a>            </li>
                                    </div>

                                    <!--<li <?php /*if($controller == 'reports') echo "class='active'";*/ ?>>            <a href="<?php /*echo(base_url('admin/reports')); */ ?>"><i class="icon-home4"></i> <span>Reports</span></a>        </li>-->
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->        </div>
                    </div>
                    <!-- /main sidebar -->        <!-- Main content -->
                    <div class="content-wrapper">            <!-- Page header -->
                        <div class="page-header">
                            <!--<div class="breadcrumb-line">                    <ul class="breadcrumb">                        <li><a href="index.html"><i class="icon-home2 position-left"></i> <?php /*echo preg_replace('/([a-z])([A-Z])/s','$1 $2', $controller);*/?></a></li>                        <li class="active"><?php /*echo ucfirst(preg_replace('/([a-z])([A-Z])/s','$1 $2', $method))*/?></li>                    </ul>                </div>            </div>-->
                            <!-- messages --> <img id="loaderImg"
                            style="display:none;left: 0;margin: auto;position: fixed;right: 0;top: 40%;z-index: 100000;"
                            src="<?php echo(base_url('assets/admin/images/ajax-loader(1).gif')); ?>"
                            alt="Loading Please Wait...!">            <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-danger" role="alert">
                                <!--<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>--> <span
                                class="sr-only">Error:</span>                    <?php echo $this->session->flashdata('error'); ?>
                            </div>            <?php } else if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-success" role="alert">
                                <!--<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>--> <span
                                class="sr-only">Success:</span>                    <?php echo $this->session->flashdata('success'); ?>
                            </div>            <?php } ?>            <!-- /page header -->
