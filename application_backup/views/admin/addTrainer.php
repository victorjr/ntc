<style>.sidebar {
        min-height: 660px;
    }</style><!--Content area -->
<div class="content"><!-- CKEditor default -->
    <div class="panel panel-flat">
        <div class="panel-heading"><h5 class="panel-title">Add Trainer Detail</h5></div>
        <div class="panel-body">
            <form method="post" class="form-horizontal" action="<?php echo(base_url('admin/trainers/addTrainer')); ?>"
                  enctype="multipart/form-data">
                <fieldset class="content-group">
                    <div class="form-group"><label class="control-label col-lg-2">First Name: </label>

                        <div class="col-lg-6">
                            <div class="error"> <?php echo form_error('firstName'); ?></div>
                            <input type="text" required="" name="firstName"
                                   value="<?php echo set_value('firstName'); ?>" class="form-control"></div>
                    </div>
                    <div class="form-group"><label class="control-label col-lg-2">Last Name: </label>

                        <div class="col-lg-6">
                            <div class="error"> <?php echo form_error('lastName'); ?></div>
                            <input type="text" name="lastName" value="<?php echo set_value('lastName'); ?>"
                                   class="form-control"></div>
                    </div>
                    <!--<div class="form-group">                    <label class="control-label col-lg-2">Username: </label>                    <div class="col-lg-6">                        <div class="error"> <?php /*echo form_error('username'); */ ?></div>                        <input type="text" required="" name="username" value="<?php /*echo set_value('username'); */ ?>" class="form-control">                    </div>                </div>                <div class="form-group">                    <label class="control-label col-lg-2">Password: </label>                    <div class="col-lg-6">                        <?php /*echo form_error('password'); */ ?>                        <input type="password" required="" id="password" name="password" value="<?php /*echo set_value('password'); */ ?>" class="form-control">                    </div>                </div>-->
                    <!--<div class="form-group">                    <label class="control-label col-lg-2">Confirm Password: </label>                    <div class="col-lg-6">                        <?php /*echo form_error('Cpassword'); */ ?>                        <input type="password" required="" id="Cpassword" name="Cpassword" value="<?php /*echo set_value('Cpassword'); */ ?>" class="form-control">                    </div>                </div>-->
                    <div class="form-group" id="programIdC"><label class="control-label col-lg-2">Training
                            Program:</label>

                        <div class="col-lg-6">
                            <div class="multi-select-full">
                                <select class="multiselect-filtering" id="programId" multiple="multiple" name="programId[]">
                                    <?php foreach ($trainings as $v) { ?>
                                        <option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="programIdQ"><label class="control-label col-lg-2">Qualifications:</label>

                        <div class="col-lg-6">
                            <div class="multi-select-full">
                                <select class="multiselect-filtering" id="qualification_id" multiple="multiple" name="qualification_ids[]">
                                    <?php foreach ($qualifications as $v) { ?>
                                        <option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group" >                    <label class="control-label col-lg-2">Training Program:</label>                    <div class="col-lg-6">                        <div class="multi-select-full">                            <select  required="" id="programId" multiple="multiple" name="programId[]" class="form-control">                                <?php foreach ($trainings as $v) { ?>                                <option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>                                <?php } ?>                            </select>                        </div>                        </div>                </div> -->
                    <div class="form-group">
                        <div class="col-lg-3">
                            <button type="submit" class="btn bg-teal-400">Submit<i
                                    class="icon-arrow-right14 position-right"></i></button>
                            <a href="<?php echo base_url('admin/trainers') ?>">
                                <button type="button" class="btn bg-teal-400">Cancel<i
                                        class="icon-arrow-right14 position-right"></i></button>
                            </a></div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <script>        $(document).ready(function () {
            var password = document.getElementById("password"), confirm_password = document.getElementById("Cpassword");

            function validatePassword() {
                if (password.value != confirm_password.value) {
                    confirm_password.setCustomValidity("Passwords Don't Match");
                } else {
                    confirm_password.setCustomValidity('');
                }
            }

            password.onchange = validatePassword;
            confirm_password.onkeyup = validatePassword;
        });    </script>
    <!-- /CKEditor default