<!DOCTYPE HTML>
<html>
<head>
    <title>NTC User Form</title>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/admin/css/minified/bootstrap.min.css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo(base_url()); ?>assets/admin/js/core/libraries/jquery.min.js"></script>
    <style>

        .banner {
            background: rgba(0, 0, 0, 0) url('http://ntc.matrix-intech.com/assets/admin/images/user-detail.png') no-repeat scroll 50% 0 / cover;
            width: 100%;
            height: 250px;
            position: relative;
            top: 0px;
            margin: 0 0 70px 0;
            text-align: center;
            color: #fff;
        }

        .banner h1 {
            display: block;
            font-size: 50px;
            font-weight: bold;
            margin: 0;
            padding: 60px 0 0;
            vertical-align: middle;

        }

        .details-form {
            max-width: 800px;
            display: block;
            margin: 0 auto;
            height: auto;
        }

        .details-form .form-style {
            height: 40px;
            padding: 0 10px;
            width: 100%;

        }

        .details-form label {
            position: relative;
            padding: 15px 0 10px;
        }

        .details-form label:after {
            display: block;
            clear: both;
            content: "";
        }

        .details-form .btn.dropdown-toggle.btn-default {
            border: 1px solid;
            border-radius: 0;
            padding: 9px;
            width: 100% !important;
            margin: 0px !important;
            box-shadow: none;
            background: none;
            color: #777;
        }

        .details-form .submit-btn {
            background: #619eb6 none repeat scroll 0 0;
            border: medium none;
            color: #f1f1f3;
            display: block;
            font-size: 20px;
            font-weight: bold;
            height: 50px;
            text-transform: uppercase;
            width: 55%;
            margin: 0 auto;
            border-radius: 2px;
            position: relative;
            top: 20px;
            margin-bottom: 40px;
        }

        .radio-inline {
            display: inline-block;
        }

        .title-label {
            display: block;
            margin: 0 0 -15px 0;
        }

        .checkbox-sec {
            margin: -10px 0 -5px;
            position: relative;
            top: 10px;
        }

        .checkbox-sec > .font {
            font-weight: normal;
        }

        .checkbox-sec > #radio2 {
            margin: 0 0 0 15px;

        }

    </style>

</head>
<body>

<div class="banner">
    <h1 class="search-jb">NTC FORM</h1>
</div>

<div class="container">
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger" role="alert">
            <!--<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>--> <span
                class="sr-only">Error:</span>                    <?php echo $this->session->flashdata('error'); ?>
        </div>            <?php } else if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success" role="alert">
            <!--<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>--> <span
                class="sr-only">Success:</span>                    <?php echo $this->session->flashdata('success'); ?>
        </div>            <?php } ?>

    <div class="details-form">
        <form id="surveyForm" action="<?php echo base_url('admin/survey/add/' . $token); ?>"
              method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="checkbox-sec">
                        <label class="title-label">1. Graduated. </label>
                        <input type="radio" class="optional" value="Yes" name="graduated" id="graduatedY"/>
                        <label for="graduatedY" class="font">
                            Yes
                        </label>
                        <input type="radio" class="optional" value="No" name="graduated" id="graduatedN"/>
                        <label for="graduatedN" class="font">
                            No
                        </label>
                    </div>
                </div>
                <div id="graduatedQ" class="col-md-12">
                    <label>2. Why did you not graduated?</label>
                    <input type="text" placeholder="" name="why_did_you_not_graduated" class="form-style">
                </div>
                <div class="col-md-12">
                    <label>3. What are you doing now</label>
                    <input type="text" placeholder="" name="what_are_you_doing_now" class="form-style">
                </div>
                <div class="col-md-12">

                    <div class="checkbox-sec">
                        <label class="title-label">4. Working. </label>
                        <input type="radio" class="optional" value="Yes" name="working" id="workingY"/>
                        <label for="workingY" class="font">
                            Yes
                        </label>
                        <input type="radio" class="optional" value="No" name="working" id="workingN"/>
                        <label for="workingN" class="font">
                            No
                        </label>
                    </div>
                </div>
                <div id="workingQ" class="col-md-12">
                    <label>5. Why are you not working?</label>
                    <input type="text" placeholder="" name="why_are_you_not_working" class="form-style">

                </div>
                <div class="col-md-12">

                    <div class="checkbox-sec">
                        <label class="title-label">6. Did employer offer job. </label>
                        <input type="radio" class="optional" value="Yes" name="did_employer_offer_job"
                               id="did_employer_offer_jobY"/>
                        <label for="did_employer_offer_jobY" class="font">
                            Yes
                        </label>
                        <input type="radio" class="optional" value="No" name="did_employer_offer_job"
                               id="did_employer_offer_jobN"/>
                        <label for="did_employer_offer_jobN" class="font">
                            No
                        </label>
                    </div>
                </div>
                <div class="col-md-12" id="did_employer_offer_jobQ">
                    <label>7. If not offer job why </label>
                    <input type="text" placeholder="" name="no_offer_why" class="form-style">
                </div>
                <div class="col-md-12">
                    <label>8. Who is your employer.</label>
                    <input type="text" placeholder="" name="who_is_employer" class="form-style">
                </div>
                <div class="col-md-12">

                    <div class="checkbox-sec">
                        <label class="title-label">9. Self Employed </label>
                        <input type="radio" value="Yes" name="self_employed" id="self_employedY"/>
                        <label for="self_employedY" class="font">
                            Yes
                        </label>
                        <input type="radio" value="No" name="self_employed" id="self_employedN"/>
                        <label for="self_employedN" class="font">
                            No
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>10. Workplace Location.</label>
                    <input type="text" placeholder="" name="work_location" class="form-style">
                </div>
                <div class="col-md-6">
                    <label>11. Job Title</label>
                    <input type="text" placeholder="" name="job_title" class="form-style">
                </div>

                <div class="col-md-12">
                    <div class="checkbox-sec">
                        <label class="title-label">12. Using skills learning in training.</label>
                        <input type="radio" value="Yes" name="using_skills_learning_in_training"
                               id="using_skills_learning_in_trainingY"/>
                        <label for="using_skills_learning_in_trainingY" class="font">
                            Yes
                        </label>
                        <input type="radio" value="No" name="using_skills_learning_in_training"
                               id="using_skills_learning_in_trainingN"/>
                        <label for="using_skills_learning_in_trainingN" class="font">
                            No
                        </label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="checkbox-sec">
                        <label class="title-label">13. Ever been promoted</label>
                        <input type="radio" value="Yes" name="ever_been_promoted" id="ever_been_promotedY"/>
                        <label for="ever_been_promotedY" class="font">
                            Yes
                        </label>
                        <input type="radio" value="No" name="ever_been_promoted" id="ever_been_promotedN"/>
                        <label for="ever_been_promotedN" class="font">
                            No
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>14. Earning more than before training.</label>
                    <input type="text" placeholder="" name="earning_more_than_before_training" class="form-style">
                </div>
                <div class="col-md-6">
                    <label>15. Already or intend to enroll in other training</label>
                    <input type="text" placeholder="" name="already_or_intend_to_enroll_in_other_training"
                           class="form-style">
                </div>
                <div class="col-md-12">
                    <label>16. If yes what type.</label>
                    <input type="text" placeholder="" name="if_yes_what_type" class="form-style">
                </div>

            </div>

            <input type="submit" value="Submit" class="submit-btn" name="form">
        </form>
    </div>
</div>
</body>
</html>
<script>
    $(document).ready(function () {
        $("#surveyForm .optional").on('change', function () {
            var identifier = $(this).attr('name');
            if ($(this).val() == 'No') {
                $("#" + identifier + "Q").show(300);
            } else {
                $("#" + identifier + "Q").hide(300);
            }
        });
    });
</script>