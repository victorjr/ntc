<?php if(count($user)>0){?>

    <!--<div class="form-group" id="ssnC">
        <label class="control-label col-lg-2">Select Enrolment:</label>
        <div class="col-lg-6">
            <select id="enrolment" name="enrolment" required="" class="form-control">
                <?php /*foreach ($enrList as $v) {*/?>
                    <option value="<?php /*echo $v['id'];*/?>"><?php /*echo $v['ssn'];*/?></option>
                <?php /*}*/?>
            </select>
        </div>
    </div>-->
    <div class="col-lg-6 col-sm-6">
        <div class="form-group">
            <label class="control-label">First Name: </label>
            <div class="">
                <input type="text" readonly required="" name="firstName" value="<?php echo $user[0]['firstName']; ?>" class="form-control">
            </div>
        </div>
    </div> 
    <div class="col-lg-6 col-sm-6">   
        <div class="form-group">
            <label class="control-label">Last Name: </label>
            <div class="">
                <input type="text" readonly required="" name="lastName" value="<?php echo $user[0]['lastName']; ?>" class="form-control">
            </div>
        </div>
    </div> 
    <div class="col-lg-6 col-sm-6">   
        <div class="form-group" id="ssnC">
            <label class="control-label">Select Enrolment:</label>
            <div class="">
                <select id="enrolment" name="enrolment" required="" class="form-control">
                    <?php foreach ($enrolledPro as $v) {?>
                        <option value="<?php echo $v['id'];?>"><?php echo $v['programTitle'];?></option>
                    <?php }?>
                </select>
            </div>
        </div>
    </div>    
    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
            <label class="control-label col-lg-2">Gender: </label>
            <div class="col-lg-6">
                <div class="col-lg-10  ">
                    <div class="col-lg-6">
                        <label class="control-label col-lg-2">Male: </label>
                        <input disabled <?php if($user[0]['gender'] == 'male') echo 'checked';?> type="radio" name="gender" value="male" class="form-control" style="position: absolute; height: 20px;top:4px;margin-left: 50px;">
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label col-lg-4">Female: </label>
                        <input disabled <?php if($user[0]['gender'] == 'female') echo 'checked';?> type="radio" name="gender" value="female" class="form-control" style="position: absolute; height: 20px;top:4px;margin-left: 60px;">
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="col-lg-6 col-sm-6">
        <div class="form-group">
            <label class="control-label"> DOB: </label>
            <div class="">
                <input readonly type="text" name="dob" id="dob" value="<?php echo $user[0]['dob']; ?>" class="form-control">
            </div>
        </div>
    </div>    
<?php }else{?>

    <!--<div class="form-group" id="ssnC">
        <label class="control-label col-lg-2">Select Enrolment:</label>
        <div class="col-lg-6">
            <select id="enrolment" name="enrolment" required="" class="form-control"></select>
        </div>
    </div>-->

    <div class="form-group">
        <label class="control-label col-lg-2">First Name: </label>
        <div class="col-lg-6">
            <input type="text" readonly required="" name="firstName" value="" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2">Last Name: </label>
        <div class="col-lg-6">
            <input type="text" readonly required="" name="lastName" value="" class="form-control">
        </div>
    </div>

    <div class="form-group" id="ssnC">
        <label class="control-label col-lg-2">Select Enrolment:</label>
        <div class="col-lg-6">
            <select id="enrolment" name="enrolment" required="" class="form-control"></select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">Gender: </label>
        <div class="col-lg-6">
            <div class="col-lg-6">
                <div class="col-lg-6">
                    <label class="control-label col-lg-2">Male: </label>
                    <input disabled type="radio" name="gender" value="male" class="form-control">
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-lg-4">Female: </label>
                    <input disabled type="radio" name="gender" value="female" class="form-control">
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2"> DOB: </label>
        <div class="col-lg-6">
            <input readonly type="text" name="dob" id="dob" value="" class="form-control">
        </div>
    </div>
<?php }?>