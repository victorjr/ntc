<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title ">Trainee Detail</h5>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                <tr>

                    <th colspan="3" class="active title">Detail</th>

                </tr>

                <tr>

                    <td class="background">First Name:</td>

                    <td><?php echo $data['firstName'];?></td>

                </tr>

                <tr>

                    <td class="background">Last Name:</td>

                    <td><?php echo $data['lastName'];?></td>

                </tr>

                <tr>

                    <td class="background">Email:</td>

                    <td><?php echo $data['email'];?></td>

                </tr>


                <tr>

                    <td class="background">Social Security Number:</td>

                    <td><?php echo $data['ssn'];?></td>

                </tr>

                <tr>

                    <td class="background">Atoll:</td>

                    <td><?php echo ucfirst($data['atollName']);?></td>

                </tr>

                <tr>

                    <td class="background">Citizenship:</td>

                    <td><?php echo ucfirst($data['citizenName']);?></td>

                </tr>

                <tr>

                    <td class="background">Enrolled Programs:</td>

                    <td><?php echo implode(' | ', array_map(function ($entry) {
                            return $entry['title'];
                        }, $prog));?>
                    </td>

                </tr>

                <tr>

                    <td class="background">Gender:</td>

                    <td><?php echo ucfirst($data['gender']);?></td>

                </tr>

                <tr>

                    <td class="background">Age:</td>

                    <td><?php echo $data['age'];?></td>

                </tr>

                <tr>

                    <td class="background">Marital Status:</td>

                    <td><?php if($data['martialStatus']) echo 'Married';else echo 'Un-Married';?></td>

                </tr>



                <tr>

                    <td class="background">DOB:</td>

                    <td><?php echo $data['dob'];?></td>

                </tr>

                <tr>

                    <td class="background">Is Employed:</td>

                    <td><?php if($data['employed']) echo 'Yes';else echo 'No';?></td>

                </tr>

                <tr>

                    <td class="background">Employed Location:</td>

                    <td><?php echo $data['employedLocation'];?></td>

                </tr>

                <tr>

                    <td class="background">Last School Attended:</td>

                    <td><?php echo $data['lastSchoolAttended'];?></td>

                </tr>

                <tr>

                    <td class="background">School Complete Date:</td>

                    <td><?php echo $data['schoolCompletedDate'];?></td>

                </tr>

                <tr>

                    <td class="background">Highest Grade:</td>

                    <td><?php echo $data['highestGrade'];?></td>

                </tr>

                <tr>

                    <td class="background">Other:</td>

                    <td><?php echo $data['other'];?></td>

                </tr>

                <tr>

                    <td class="background">Created:</td>

                    <td><?php echo date('M d,Y H:i',strtotime($data['created']));?></td>

                </tr>



                </tbody>

            </table>

        </div>

    </div>

</div>