<style>.dash-box.green {
        background: #2ecc71 none repeat scroll 0 0;
    }

    .dash-box.blue {
        background: #619eb6 none repeat scroll 0 0;
    }

    .dash-box.pnk {
        background: #9b59b6 none repeat scroll 0 0;
    }

    .dash-box {
        margin: 0 0 30px;
        overflow: hidden;
        padding: 15px;
        position: relative;
    }

    .dash-box .icon-holder {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        border-radius: 50%;
        color: #ededed;
        float: left;
        font-size: 40px;
        height: 80px;
        line-height: 73px;
        text-align: center;
        width: 80px;
    }

    .dash-box .dash-content {
        color: #ededed;
        float: right;
    }

    .dash-content strong {
        display: block;
        font-size: 56px;
        font-weight: 700;
        line-height: 60px;
        text-align: center;
    }

    .dash-content h2 {
        font-weight: 700;
    }

    .bg-dashboard {
        background: #eee;
    }
</style><!-- Content area -->
<div class="content bg-dashboard">
    <div class="dashboard-content">
        <div class="three-column">
            <div class="col-md-4"><a href="<?php echo base_url('admin/programs'); ?>">
                    <div class="dash-box green">
                        <div class="icon-holder"><i class="fa fa-list-alt" aria-hidden="true"></i></div>
                        <div class="dash-content"><strong><?php echo $programs; ?></strong>

                            <h2>Training Programs</h2></div>
                    </div>
                </a></div>
            <div class="col-md-4"><a href="<?php echo base_url('admin/trainees'); ?>">
                    <div class="dash-box blue">
                        <div class="icon-holder"><i class="fa fa-user" aria-hidden="true"></i></div>
                        <div class="dash-content"><strong><?php echo $trainees; ?></strong>

                            <h2>Trainees</h2></div>
                    </div>
                </a> <!-- Box End -->            </div>
            <div class="col-md-4"><a href="<?php echo base_url('admin/job_corps'); ?>">
                    <div class="dash-box pnk">
                        <div class="icon-holder"><i class="fa fa-home" aria-hidden="true"></i></div>
                        <div class="dash-content"><strong><?php echo $jobCorps; ?></strong>

                            <h2>Job Corps</h2></div>
                    </div>
                </a> <!-- Box End -->            </div>
        </div>
        <!-- Three Columns -->
        <div class="map-holder">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading"><h4 class="panel-title">Training program category chart:</h4></div>
                    <div class="panel-body">
                        <div class="chart-container has-scroll">
                            <div class="chart has-fixed-height has-minimum-width" id="basic_pie"></div>
                        </div>
                    </div>
                </div>
                <!-- Basic column chart -->
                <div class="panel panel-flat">
                    <div class="panel-heading"><h5 class="panel-title">Trainee home atoll</h5></div>
                    <div class="panel-body">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="basic_columns"></div>
                        </div>
                    </div>
                </div>
                <!-- /basic column chart -->            </div>
        </div>
        <!-- map-holder -->    </div>
</div>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/admin/js/plugins/visualization/echarts/echarts.js"></script>
<!--<script type="text/javascript" src="<?php /*echo(base_url()); */?>assets/admin/js/charts/echarts/columns_waterfalls.js"></script>-->
<!--<script type="text/javascript" src="<?php /*echo(base_url()); */?>assets/admin/js/charts/echarts/timeline_option.js"></script>-->
<!--<script type="text/javascript" src="<?php /*echo(base_url()); */?>assets/admin/js/charts/echarts/pies_donuts.js"></script>-->
<script>
    /* ------------------------------------------------------------------------------
     *
     *  # Echarts - pies and donuts
     *
     *  Pies and donuts chart configurations
     *
     *  Version: 1.0
     *  Latest update: August 1, 2015
     *
     * ---------------------------------------------------------------------------- */

    $(function () {
        // Set paths
        // ------------------------------

        require.config({
            paths: {
                echarts: '<?php echo base_url();?>assets/admin/js/plugins/visualization/echarts'
            }
        });

        // Configuration
        // ------------------------------

        require(
            [
                'echarts',
                'echarts/theme/limitless',
                'echarts/chart/pie',
                'echarts/chart/funnel',
                'echarts/chart/bar',
                'echarts/chart/line'
            ],


            // Charts setup
            function (ec, limitless) {


                // Initialize charts
                // ------------------------------

                var basic_pie = ec.init(document.getElementById('basic_pie'), limitless);
                var basic_columns = ec.init(document.getElementById('basic_columns'), limitless);


                // Charts setup
                // ------------------------------

                //
                // Basic pie options
                //

                basic_pie_options = {

                    // Add title
                    /* title: {
                     text: 'Total Users: <?php //echo count($users);?>',
                     //subtext: 'Open source information',
                     x: 'center'
                     },*/

                    // Add tooltip
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}: {c} ({d}%)"
                    },

                    // Add legend
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        data: [<?php //.echo '"'.implode('","', array_column($options, 'title')).'"' ?>]
                    },
                    // Add series
                    series: [{
                        //name: 'Question:',
                        type: 'pie',
                        radius: '70%',
                        //center: ['50%', '57.5%'],
                        data: [
                            <?php foreach ($report as $k=>$val){
                                echo "{value: ".$val.", name: '".$k."'},";
                                }
                            ?>
                            //{value: '335', name: 'abc'},
                            //{value: 310, name: 'xyz'}
                        ]
                    }]
                };


                basic_columns_options = {

                    // Setup grid
                    grid: {
                        x: 40,
                        x2: 40,
                        y: 35,
                        y2: 25
                    },

                    // Add tooltip
                    tooltip: {
                        trigger: 'axis'
                    },

                    // Add legend
                    legend: {
                        data: ['Trainees']
                    },

                    // Enable drag recalculate
                    calculable: true,

                    // Horizontal axis
                    xAxis: [{
                        type: 'category',
                        data: [<?php echo '"'.implode('","',array_keys($traineeReport)).'"';?>]
                    }],

                    // Vertical axis
                    yAxis: [{
                        type: 'value'
                    }],

                    // Add series
                    series: [
                        {
                            name: 'Trainees',
                            type: 'bar',
                            data: [<?php echo '"'.implode('","',($traineeReport)).'"';?>],
                            itemStyle: {
                                normal: {
                                    label: {
                                        show: true,
                                        textStyle: {
                                            fontWeight: 500
                                        }
                                    }
                                }
                            },
                            markLine: {
                                data: [{type: 'average', name: 'Average'}]
                            }
                        }
                    ]
                };




                // Apply options
                // ------------------------------

                basic_pie.setOption(basic_pie_options);
                basic_columns.setOption(basic_columns_options);
            }
        );
    });
</script>