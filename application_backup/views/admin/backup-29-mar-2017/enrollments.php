<!-- Content area -->
<div class="content">
    <!-- CKEditor default -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div>
                <h5 class="panel-title"><?php echo @$contentP[0]['title']?></h5>
            </div>
            <div style="padding-top: 15px;">
                <a href="<?php echo base_url('admin/enrollments/addEnrollment/'.$id)?>">
                    <button class="btn btn-success" type="button">Add New Enrollment</button>
                </a>
            </div>
        </div>

        <div class="panel-body">
            <form action="<?php echo base_url('admin/enrollments/index/'.$id)?>" method="get" >
                <div class="datatable-header">
                    <div id="DataTables_Table_4_filter" class="dataTables_filter">
                        <label><span>Filter:</span>
                            <input style="width: 380px;" class="" value="<?php echo @$q;?>" name="q" placeholder="Trainee" aria-controls="DataTables_Table_4" type="search">
                        </label>
                        <button class="btn btn-default" type="submit">Search</button>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-hover datatable-highlight">
                <thead>
                <tr>
                    <!--<th>Training Program</th>-->
                    <th>Trainee</th>
                    <th>Join date</th>
                    <th>Creation Date</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($content as $val) {?>
                    <tr>
                        <!--<td><?php /*echo ucfirst($val['title'])*/?></td>-->
                        <td><?php echo ucfirst($val['firstName'].' '.$val['lastName'])?></td>
                        <td><?php echo ucfirst(date('M d, Y',(strtotime($val['appliedDate']))))?></td>
                        <td><?php echo ucfirst(date('M d, Y',(strtotime($val['created']))))?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="<?php echo(base_url('admin/enrollments/enrollmentDetail/'.$val['id'].'/'.$id)); ?>"><i class="fa fa-info"></i> Detail</a></li>
                                        <li><a href="<?php echo(base_url('admin/enrollments/editEnrollment/'.$val['id'].'/'.$id)); ?>"><i class="fa fa-pencil"></i> Edit</a></li>
                                        <li><a onclick="return confirm('Are you sure?');" href="<?php echo(base_url('admin/enrollments/deleteEnrollment/'.$val['id'].'/'.$id)); ?>"><i class="fa fa-trash"></i> Delete</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="datatable-footer">
                <div class="dataTables_info" id="DataTables_Table_4_info" role="status" aria-live="polite"><?php echo $dataInfo;?></div>
                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_4_paginate">
                    <?php echo $links;?>
                    <!--<a class="paginate_button previous disabled" aria-controls="DataTables_Table_4" data-dt-idx="0" tabindex="0" id="DataTables_Table_4_previous">←</a>
                    <span><a class="paginate_button current" aria-controls="DataTables_Table_4" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="DataTables_Table_4" data-dt-idx="2" tabindex="0">2</a></span>
                    <a class="paginate_button next" aria-controls="DataTables_Table_4" data-dt-idx="3" tabindex="0" id="DataTables_Table_4_next">→</a>-->
                </div>
            </div>
        </div>
    </div>