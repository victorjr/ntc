<style>
   
</style>
<!-- Content area -->
<div class="content">



<!-- CKEditor default -->

<div class="panel panel-flat">

    <div class="panel-heading">
        <h5 class="panel-title">Add Trainee Detail</h5>
    </div>



    <div class="panel-body">

        <form method="post" id="traineeAdd" class="form-horizontal" action="<?php echo(base_url('admin/trainees/addTrainee')); ?>" enctype="multipart/form-data">

            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2">First Name: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('firstName'); ?></div>
                        <input type="text" required="" name="firstName" value="<?php echo set_value('firstName'); ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Last Name: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('lastName'); ?></div>
                        <input type="text" required="" name="lastName" value="<?php echo set_value('lastName'); ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Email: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('email'); ?></div>
                        <input type="email" required="" name="email" value="<?php echo set_value('email'); ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Social Security Number: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('ssn'); ?></div>
                        <input type="text" name="ssn" value="<?php echo set_value('ssn'); ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Gender: </label>
                    <div class="col-lg-6">
                        <div class="col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label col-lg-2">Male: </label>
                                <input style=" position: relative;left: 20px;" type="radio" checked name="gender" value="male" class="form-control margin-left">
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-lg-4">Female: </label>
                                <input style=" position: relative;left: 20px;" type="radio" name="gender" value="female" class="form-control margin-left">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Age: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('age'); ?></div>
                        <input type="number" min="1" name="age" value="<?php echo set_value('age'); ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Martial Status: </label>
                    <div class="col-lg-6">
                        <div class="col-lg-6">
                            <label class="control-label col-lg-2">Married: </label>
                            <input style=" position: relative;left: 22px;" type="radio" checked name="martialStatus" value="1" class="form-control">
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label col-lg-4" style="width:100%;">Un-Married: </label>
                            <input style=" position: relative;left: 25px;" type="radio" name="martialStatus" value="0" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group" id="atollC">
                    <label class="control-label col-lg-2">Atoll:</label>
                    <div class="col-lg-6">
                        <select id="homeAtoll" name="homeAtoll" class="form-control">
                            <?php foreach ($atolls as $v) {?>
                            <option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="citizenshipC">
                    <label class="control-label col-lg-2">Citizenship:</label>
                    <div class="col-lg-6">
                        <select id="citizenship" name="citizenship" class="form-control">
                            <?php foreach ($citizenship as $v) {?>
                                <option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2"> DOB: </label>
                    <div class="col-lg-6">
                        <?php echo form_error('dob'); ?>
                        <input type="text" name="dob" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control pickadate-max-limits">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Place Of Birth: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('placeOfBirth'); ?></div>
                        <input type="text" name="placeOfBirth" value="<?php echo set_value('placeOfBirth'); ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Is Employed?: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('employed'); ?></div>
                        <input type="checkbox" name="employed" value="1" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Employed Location: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('employedLocation'); ?></div>
                        <input type="text" name="employedLocation" value="<?php echo set_value('employedLocation'); ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Last School Attended: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('lastSchoolAttended'); ?></div>
                        <input type="text" name="lastSchoolAttended" value="<?php echo set_value('lastSchoolAttended'); ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">School Complete Date: </label>
                    <div class="col-lg-6">
                        <?php echo form_error('schoolCompletedDate'); ?>
                        <input type="text" name="schoolCompletedDate" id="schoolCompletedDate" value="<?php echo set_value('schoolCompletedDate'); ?>" class="form-control pickadate-max-limits">
                    </div>
                </div>


                <!--<div class="form-group">
                    <label class="control-label col-lg-2">Highest Grade Completed: </label>
                    <div class="col-lg-6">
                        <div class="col-lg-6">
                            <div class="col-lg-6">
                                <label class="control-label col-lg-2">Yes: </label>
                                <input  style=" position: relative;left: 20px;" type="radio" name="highestGrade" value="Yes" class="highestGrade form-control margin-left">
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-lg-4">No: </label>
                                <input  style=" position: relative;left: 20px;" type="radio" name="highestGrade" value="No" class="highestGrade form-control margin-left">
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-lg-4">Other: </label>
                                <input  style=" position: relative;left: 20px;" type="radio" checked name="highestGrade" value="Other" class="highestGrade form-control margin-left">
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="form-group" id="gradeC">
                    <label class="control-label col-lg-2">Highest grade level completed:</label>
                    <div class="col-lg-6">
                        <select id="grade" name="highestGrade" class="form-control">
                            <option value="Elementary">Elementary</option>
                            <option value=" H.S. Dropout"> H.S. Dropout </option>
                            <option value="High School">High School</option>
                            <option value="High School (w/ vocational studies)"> High School (w/ vocational studies)</option>
                            <option value="2-yrs College"> 2-yrs College </option>
                            <option value="4-yrs College">4-yrs College</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="control-label col-lg-2">Highest Grade Completed: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php /*echo form_error('highestGrade'); */?></div>
                        <input type="text" name="highestGrade" value="<?php /*echo set_value('highestGrade'); */?>" class="form-control">
                    </div>
                </div>-->

                <div class="form-group" id="otherB" style="display: none;">
                    <label class="control-label col-lg-2">Other - Highest Grade Completed: </label>
                    <div class="col-lg-6">
                        <div class="error"> <?php echo form_error('other'); ?></div>
                        <input type="text" id="other" name="other" value="<?php echo set_value('other'); ?>" class="form-control">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-lg-3">
                        <button type="submit" class="btn bg-teal-400">Submit<i class="icon-arrow-right14 position-right"></i></button>
                        <a href="<?php echo base_url('admin/trainees')?>">
                            <button type="button" class="btn bg-teal-400">Cancel<i class="icon-arrow-right14 position-right"></i></button>
                        </a>
                    </div>
                </div>
            </fieldset>

        </form>

    </div>

</div>
<script>
    $(document).ready(function () {
        $("#traineeAdd #grade").on('change', function () {
            if ($(this).val() == 'Other') {
                $('#otherB').show(300);
            } else {
                $('#otherB').hide(300);
                $('#other').val(" ");
            }
        });
    });
</script>

<!-- /CKEditor default -->