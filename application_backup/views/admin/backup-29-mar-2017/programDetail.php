<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title">Training Program Detail</h5>

            <a class="pull-right" href="<?php echo base_url('admin/enrollments/index/'.$data['id'])?>">

                <button class="btn btn-success" type="button">Enroll Trainee</button>

            </a>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                <tr>

                    <th colspan="3" class="active title">Detail</th>

                </tr>

                <tr>

                    <td class="background">Title:</td>

                    <td><?php echo $data['title'];?></td>

                </tr>

                <tr>

                    <td class="background">Contact:</td>

                    <td><?php echo $data['contact'];?></td>

                </tr>

                <tr>

                    <td class="background">Organization:</td>

                    <td><?php echo $data['organization'];?></td>

                </tr>

                <tr>

                    <td class="background">Location:</td>

                    <td><?php echo $data['location'];?></td>

                </tr>

                <tr>

                    <td class="background">Trainers:</td>

                    <td><?php echo $trainerData;?></td>

                </tr>

                <tr>

                    <td class="background">Category:</td>

                    <td><?php echo $data['catName'];?></td>

                </tr>

                <tr>

                    <td class="background">Established:</td>

                    <td><?php if($data['established']) echo 'Yes';else echo 'No';?></td>

                </tr>

                <tr>

                    <td class="background">Academic Credit:</td>

                    <td><?php if($data['academicCredit']) echo 'Yes';else echo 'No';?></td>

                </tr>

                <tr>

                    <td class="background">Certification:</td>

                    <td><?php if($data['certification']) echo 'Yes';else echo 'No';?></td>

                </tr>

                <tr>

                    <td class="background">NRW Fund:</td>

                    <td><?php echo $data['nrwFund'];?></td>

                </tr>

                <tr>

                    <td class="background">SEG Fund:</td>

                    <td><?php echo $data['segFund'];?></td>

                </tr>

                <tr>

                    <td class="background">Approved Amount:</td>

                    <td><?php echo $data['amountApproved'];?></td>

                </tr>

                <tr>

                    <td class="background">Requested Amount:</td>

                    <td><?php echo $data['amountRequested'];?></td>

                </tr>

                <tr>

                    <td class="background">Duration:</td>

                    <td><?php echo $data['duration'];?></td>

                </tr>

                <tr>

                    <td class="background">Start Date:</td>

                    <td><?php echo $data['startDate'];?></td>

                </tr>

                <tr>

                    <td class="background">EndDate:</td>

                    <td><?php echo $data['endDate'];?></td>

                </tr>

                <tr>

                    <td class="background">Created:</td>

                    <td><?php echo date('M d,Y H:i',strtotime($data['created']));?></td>

                </tr>



                </tbody>

            </table>

        </div>

    </div>

</div>