<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">

            <h5 class="panel-title">Job Corp Detail</h5>

        </div>

        <div class="panel-body">

            <table class="table table-lg">

                <tbody>

                <tr>

                    <th colspan="3" class="active title">Detail</th>

                </tr>

                <tr>

                    <td class="background">Social Security Number:</td>

                    <td><?php echo $data['ssn'];?></td>

                </tr>

                <tr>

                    <td class="background">First Name:</td>

                    <td><?php echo $data['firstName'];?></td>

                </tr>

                <tr>

                    <td class="background">Last Name:</td>

                    <td><?php echo $data['lastName'];?></td>

                </tr>

                <tr>

                    <td class="background">Gender:</td>

                    <td><?php echo $data['gender'];?></td>

                </tr>

                <tr>

                    <td class="background">Trade:</td>

                    <td><?php echo $data['tradeTitle'];?></td>

                </tr>

                <tr>

                    <td class="background">Terminated:</td>

                    <td><?php if($data['terminated']) echo 'Yes';else echo 'No';?></td>

                </tr>

                <tr>

                    <td class="background">Sent Reminder:</td>

                    <td><?php if($data['sentReminder']) echo 'Yes';else echo 'No';?></td>

                </tr>

                <tr>

                    <td class="background">Date Apply:</td>

                    <td><?php echo date('M d,Y',strtotime($data['dateApply']));?></td>

                </tr>

                <tr>

                    <td class="background">Intake Date:</td>

                    <td><?php echo date('M d,Y',strtotime($data['intakeDate']));?></td>

                </tr>
                <tr>

                    <td class="background">Exit Date:</td>

                    <td><?php echo date('M d,Y',strtotime($data['exitDate']));?></td>

                </tr>

                <tr>

                    <td class="background">Follow UpDate:</td>

                    <td><?php echo date('M d,Y',strtotime($data['followUpDate']));?></td>

                </tr>

                <tr>

                    <td class="background">Date Apply:</td>

                    <td><?php echo date('M d,Y',strtotime($data['dateApply']));?></td>

                </tr>

                <tr>

                    <td class="background">Remarks:</td>

                    <td><?php echo $data['remarks'];?></td>

                </tr>




                </tbody>

            </table>

        </div>

    </div>

</div>