<?php $today_date = date('Y-m-d');?>
<div style="text-align:center; background:#0C9; padding:2px;">
    <h2 class="panel-title">National Training Council</h2>
    <h3 class="panel-title">Program Summary Report</h3>
</div>
<div style="text-align:center; padding-bottom:30px;">Report for Period of <?php echo $startdate;?> to <?php echo $enddate;?></div>
<?php if( !empty($content) ){ 

        $overall_total_amoun_requested = 0;
        $overall_total_amount_approved = 0;
        $overall_total_trainees = 0;
        $overall_total_trainees_male = 0;
        $overall_total_trainees_female = 0;
?>
<table style="width:100%; border:1px solid;" border="1" cellpadding="5" cellspacing="5">
    <?php foreach ($content as $cat_id=>$cat_rows) { ?>
        <tr style="background-color:#eee;">
            <th colspan="9">
                <h4 class="panel-title"><?php echo $categories[$cat_id]?>:</h4>
            </th>
        </tr>
        
        <tr>
            <th rowspan="2">Program Name</th>
            <th rowspan="2">Request Amount</th>
            <th rowspan="2">Approved Amount</th>
            <th rowspan="2">Starting Date</th>
            <th rowspan="2">Ending Date</th>
            <th rowspan="2">No. of Trainees</th>
            <th colspan="2">Gender</th>
            <th rowspan="2">Program Status</th>
        </tr>
        <tr>
            <th>Male</th>
            <th>Female</th>
        </tr>
    
        
            <?php 
                $total_amoun_requested = 0;
                $total_amount_approved = 0;
                $total_trainees = 0;
                $total_trainees_male = 0;
                $total_trainees_female = 0;
                
                foreach($cat_rows as $row){
                    $total_amoun_requested += $row['amountRequested'];
                    $total_amount_approved += $row['amountApproved'];
                    $total_trainees += $row['total_trainees'];
                    $total_trainees_male += $row['male_count'];
                    $total_trainees_female += $row['female_count'];
            ?>
            <tr>
                <td><?php echo ucfirst($row['title']) ?></td>
                <td><?php echo $row['amountRequested'] ?></td>
                <td><?php echo $row['amountApproved'] ?></td>
                <td><?php echo ucfirst(date('M d, Y', (strtotime($row['startDate'])))) ?></td>
                <td><?php echo ucfirst(date('M d, Y', (strtotime($row['endDate'])))) ?></td>
                <td><?php echo $row['total_trainees'];?></td>
                <td><?php echo $row['male_count'];?></td>
                <td><?php echo $row['female_count'];?></td>
                <td><?php echo ($today_date > $row['endDate']) ? 'Complete' : 'Ongoing';?></td>
            </tr>
            <?php } 
                $overall_total_amoun_requested += $total_amoun_requested;
                $overall_total_amount_approved += $total_amoun_requested;
                $overall_total_trainees += $total_trainees;
                $overall_total_trainees_male += $total_trainees_male;
                $overall_total_trainees_female += $total_trainees_female
            ?>
            <tr>
                <td colspan="9">
                    <?php echo $categories[$cat_id]; ?> (<?php echo count($cat_rows);?> detail <?php echo (count($cat_rows) > 1) ? 'records' : 'record';?>)
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold; font-size:15px;">Sub Totals</td>
                <td><?php echo $total_amoun_requested;?></td>
                <td><?php echo $total_amount_approved;?></td>
                <td colspan="2"></td>
                <td><?php echo $total_trainees;?></td>
                <td><?php echo $total_trainees_male;?></td>
                <td><?php echo $total_trainees_female;?></td>
                <td></td>
            </tr>
            <tr>
                <td style="font-weight:bold; font-size:15px;">Percentage</td>
                <td colspan="5"></td>
                <td><?php echo ($total_trainees > 0) ? round(($total_trainees_male/$total_trainees) *100, 2) : '0';?>%</td>
                <td><?php echo ($total_trainees > 0) ? round( ($total_trainees_female/$total_trainees) *100, 2) : '0';?>%</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="padding-bottom:20px; border:none;"></td>
            </tr>
 
<?php } ?>

       
        <tr>
            <td style="font-weight:bold; font-size:18px;">Grand Totals</td>
            <td><?php echo $overall_total_amoun_requested;?></td>
            <td><?php echo $overall_total_amount_approved;?></td>
            <td colspan="2"></td>
            <td><?php echo $overall_total_trainees;?></td>
            <td><?php echo $overall_total_trainees_male;?></td>
            <td><?php echo $overall_total_trainees_female;?></td>
            <td></td>
        </tr>
        <tr>
            <td style="font-weight:bold; font-size:18px;">Percentage</td>
            <td colspan="5"></td>
            <td><?php echo ($overall_total_trainees > 0) ? round( ($overall_total_trainees_male/$overall_total_trainees) *100, 2) : '0';?>%</td>
            <td><?php echo ($overall_total_trainees > 0) ? round( ($overall_total_trainees_female/$overall_total_trainees) *100, 2) : '0';?>%</td>
            <td></td>
        </tr>

</table>

<?php }else{?>
	No record found
<?php }?>
