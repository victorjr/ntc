<div style="text-align:center; background:#0C9; padding:2px;">
    <h2 class="panel-title">National Training Council</h2>
    <h3 class="panel-title">Training Summary Report</h3>
</div>
<div style="text-align:center; padding-bottom:30px;">Report for Period of <?php echo $startdate;?> to <?php echo $enddate;?></div>

<?php if(count($content)>0){ 

        $overall_total_male = 0;
        $overall_total_female= 0;
        $overall_total_trainees = 0;
        $overall_total_complete = 0;
        $overall_total_incomplete = 0;
?>
<table style="width:100%; border:1px solid;" border="1" cellpadding="5" cellspacing="5">
    <?php foreach ($content as $cat_id=>$cat_rows) { ?>
            <tr style="background-color:#eee;">
                <th colspan="8">
                    <h4 class="panel-title"><?php echo $categories_list[$cat_id]?>:</h4>
                </th>
            </tr>
            
            <tr>
                <th>Program Name</th>
                <th>Male</th>
                <th>Female</th>
                <th>Total Trainee</th>
                <th>Complete</th>
                <th>Incomplete</th>
                <th>Female %</th>
                <th>Complete %</th>
            </tr>
        
            <?php 
                $total_male = 0;
                $total_female= 0;
                $total_trainees = 0;
                $total_complete = 0;
                $total_incomplete = 0;
                $total_female_percent = 0;
                $total_complete_percent = 0;
                        
                foreach($cat_rows as $row){
                    $total_male += $row['male_count'];
                    $total_female += $row['female_count'];
                    $total_trainees += $row['male_count'] + $row['female_count'];
                    $total_complete += $row['complete_count'];
                    $total_incomplete += $row['incomplete_count'];
            ?>
            <tr>
                <td><?php echo ucfirst($row['title']) ?></td>
                <td><?php echo $row['male_count'];?></td>
                <td><?php echo $row['female_count'];?></td>
                <td><?php echo $row['male_count'] + $row['female_count'];?></td>
                <td><?php echo $row['complete_count'];?></td>
                <td><?php echo $row['incomplete_count'];?></td>
                <td><?php echo round( ($row['female_count'] / ($row['male_count'] + $row['female_count']) ) * 100, 2);?>%</td>
                <td><?php echo round( ($row['complete_count'] / ($row['complete_count'] + $row['incomplete_count']) ) * 100, 2);?>%</td>
            </tr>
            <?php } 
                $overall_total_male += $total_male;
                $overall_total_female += $total_female;
                $overall_total_trainees += $total_trainees;
                $overall_total_complete += $total_complete;
                $overall_total_incomplete += $total_incomplete;
            ?>
            <tr>
                <td colspan="8">
                    <?php echo $categories_list[$cat_id]?> (<?php echo count($cat_rows)?> detail <?php echo (count($cat_rows) > 1) ? 'records' : 'record';?>)
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold; font-size:15px;">Sub Totals</td>
                <td><?php echo $total_male;?></td>
                <td><?php echo $total_female;?></td>
                <td><?php echo $total_trainees?></td>
                <td><?php echo $total_complete;?></td>
                <td><?php echo $total_incomplete;?></td>
                <td><?php echo round( ($total_female / ($total_female + $total_male) ) * 100, 2);?>%</td>
                <td><?php echo round( ($total_complete / ($total_complete + $total_incomplete) ) * 100, 2);?>%</td>
            </tr>
            <tr>
                <td colspan="9" style="padding-bottom:20px; border:none;"></td>
            </tr>
    <?php } ?>
    
        <tbody>
           
            <tr>
                <td style="font-weight:bold; font-size:18px;">Grand Totals</td>
                <td><?php echo $overall_total_male;?></td>
                <td><?php echo $overall_total_female;?></td>
                <td><?php echo $overall_total_trainees?></td>
                <td><?php echo $overall_total_complete;?></td>
                <td><?php echo $overall_total_incomplete;?></td>
                <td><?php echo round( ($overall_total_female / ($overall_total_female + $overall_total_male) ) * 100, 2);?>%</td>
                <td><?php echo round( ($overall_total_complete / ($overall_total_complete + $overall_total_incomplete) ) * 100, 2);?>%</td>
            </tr>
        </tbody>
</table>

<?php }else{?>
    <h6>No record found</h6>
<?php }?>
