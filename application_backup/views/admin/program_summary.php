<?php $today_date = date('Y-m-d');?>
<style>
.tdSubTitle{
	font-weight:bold;
	font-size:13px;
}
.tdGrandTitle{
	font-weight:bold;
	font-size:16px;
}
</style>
<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">
            <h3 class="panel-title">Program Summary</h3>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">

                    <form method="post" class="form-horizontal" action="<?php echo(base_url('admin/reports/program_summary')); ?>">

                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-2"> Start Date: </label>
                                <div class="col-lg-6">
                                    <input type="text" required="" name="startdate" id="datepicker" value="<?php if(isset($startdate)) echo $startdate;?>" class="form-control pickadate-max-limits">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2"> End Date: </label>
                                <div class="col-lg-6">
                                    <input type="text" required="" name="enddate" id="datepicker2" value="<?php if(isset($enddate)) echo $enddate;?>" class="form-control pickadate-max-limits">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-lg-2"> Category: </label>
                                <div class="col-lg-6">
                                    <select name="category_id" class="form-control">
                                    	<option value="">All</option>
                                    <?php foreach($categories_list as $cat_id=>$cat_title){?>
                                    		<option <?php if(isset($category_id) && $category_id == $cat_id){ echo 'selected="selected"';}?> value="<?php echo $cat_id;?>"><?php echo $cat_title;?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php if(isset($search) && $search){?>
                                    <div class="col-lg-2">
                                        <button type="submit" name="excel" class="btn bg-teal-400">Export Excel<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>

                                    <div class="col-lg-2">
                                        <button type="submit" name="pdf" class="btn bg-teal-400">Export PDF<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                <?php }?>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn bg-teal-400">Search<i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <!-- Basic pie chart -->
                    <div class="panel panel-flat">
                        
                        <div class="panel-body">
                            <?php if(count($content)>0){ 
							
									$overall_total_amoun_requested = 0;
									$overall_total_amount_approved = 0;
									$overall_total_trainees = 0;
									$overall_total_trainees_male = 0;
									$overall_total_trainees_female = 0;
							?>
							<table class="table table-responsive table-bordered table-hover datatable-highlight">                        
                                <?php foreach ($content as $cat_id=>$cat_rows) { ?>
                                        
                                        <thead>
                                            <tr>
                                                <th colspan="9">
                                                    <h4 class="panel-title"><?php echo $categories_list[$cat_id]?>:</h4>
                                                </th>
											</tr>
                                            
                                            <tr>
                                                <th rowspan="2">Program Name</th>
                                                <th rowspan="2">Request Amount</th>
                                                <th rowspan="2">Approved Amount</th>
                                                <th rowspan="2">Starting Date</th>
                                                <th rowspan="2">Ending Date</th>
                                                <th rowspan="2">No. of Trainees</th>
                                                <th colspan="2">Gender</th>
                                                <th rowspan="2">Program Status</th>
                                            </tr>
                                            <tr>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php 
												$total_amoun_requested = 0;
												$total_amount_approved = 0;
												$total_trainees = 0;
												$total_trainees_male = 0;
												$total_trainees_female = 0;
												
												foreach($cat_rows as $row){
													$total_amoun_requested += $row['amountRequested'];
													$total_amount_approved += $row['amountApproved'];
													$total_trainees += $row['total_trainees'];
													$total_trainees_male += $row['male_count'];
													$total_trainees_female += $row['female_count'];
											?>
                                            <tr>
                                                <td><?php echo ucfirst($row['title']) ?></td>
                                                <td><?php echo $row['amountRequested'] ?></td>
                                                <td><?php echo $row['amountApproved'] ?></td>
                                                <td><?php echo ucfirst(date('M d, Y', (strtotime($row['startDate'])))) ?></td>
                                                <td><?php echo ucfirst(date('M d, Y', (strtotime($row['endDate'])))) ?></td>
                                                <td><?php echo $row['total_trainees'];?></td>
                                                <td><?php echo $row['male_count'];?></td>
                                                <td><?php echo $row['female_count'];?></td>
                                                <td><?php echo ($today_date > $row['endDate']) ? 'Complete' : 'Ongoing';?></td>
                                            </tr>
                                            <?php } 
												$overall_total_amoun_requested += $total_amoun_requested;
												$overall_total_amount_approved += $total_amoun_requested;
												$overall_total_trainees += $total_trainees;
												$overall_total_trainees_male += $total_trainees_male;
												$overall_total_trainees_female += $total_trainees_female
											?>
                                            <tr>
                                            	<td colspan="9">
                                                	<?php echo $categories_list[$cat_id]?> (<?php echo count($cat_rows)?> detail <?php echo (count($cat_rows) > 1) ? 'records' : 'record';?>)
                                                </td>
                                            </tr>
                                            <tr>
                                            	<td class="tdSubTitle">Sub Totals</td>
                                                <td><?php echo $total_amoun_requested;?></td>
                                                <td><?php echo $total_amount_approved;?></td>
                                                <td colspan="2"></td>
                                                <td><?php echo $total_trainees;?></td>
                                                <td><?php echo $total_trainees_male;?></td>
                                                <td><?php echo $total_trainees_female;?></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                            	<td class="tdSubTitle">Percentage</td>
                                                <td colspan="5"></td>
                                                <td><?php echo ($total_trainees > 0) ? round(($total_trainees_male/$total_trainees) *100, 2) : '0';?>%</td>
                                                <td><?php echo ($total_trainees > 0) ? round( ($total_trainees_female/$total_trainees) *100, 2) : '0';?>%</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="9" style="padding-bottom:20px; border:none;">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                 
                                <?php } ?>
                                
                                	<tbody>
                                       
                                        <tr>
                                            <td class="tdGrandTitle">Grand Totals</td>
                                            <td><?php echo $overall_total_amoun_requested;?></td>
                                            <td><?php echo $overall_total_amount_approved;?></td>
                                            <td colspan="2"></td>
                                            <td><?php echo $overall_total_trainees;?></td>
                                            <td><?php echo $overall_total_trainees_male;?></td>
                                            <td><?php echo $overall_total_trainees_female;?></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="tdGrandTitle">Percentage</td>
                                            <td colspan="5"></td>
                                            <td><?php echo ($overall_total_trainees > 0) ? round( ($overall_total_trainees_male/$overall_total_trainees) *100, 2) : '0';?>%</td>
                                            <td><?php echo ($overall_total_trainees > 0) ? round( ($overall_total_trainees_female/$overall_total_trainees) *100, 2) : '0';?>%</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
							</table>
                            
							<?php }else{?>
                                <h6>No record found</h6>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>