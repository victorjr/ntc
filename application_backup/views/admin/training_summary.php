<style>
.tdSubTitle{
	font-weight:bold;
	font-size:13px;
}
.tdGrandTitle{
	font-weight:bold;
	font-size:16px;
}
</style>
<div class="content">

    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">
            <h3 class="panel-title">Training Summary</h3>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">

                    <form method="post" class="form-horizontal" action="<?php echo(base_url('admin/reports/training_summary')); ?>">

                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-2"> Start Date: </label>
                                <div class="col-lg-6">
                                    <input type="text" required="" name="startdate" id="datepicker" value="<?php if(isset($startdate)) echo $startdate;?>" class="form-control pickadate-max-limits">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2"> End Date: </label>
                                <div class="col-lg-6">
                                    <input type="text" required="" name="enddate" id="datepicker2" value="<?php if(isset($enddate)) echo $enddate;?>" class="form-control pickadate-max-limits">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-lg-2"> Category: </label>
                                <div class="col-lg-6">
                                    <select name="category_id" class="form-control">
                                    	<option value="">All</option>
                                    <?php foreach($categories_list as $cat_id=>$cat_title){?>
                                    		<option <?php if(isset($category_id) && $category_id == $cat_id){ echo 'selected="selected"';}?> value="<?php echo $cat_id;?>"><?php echo $cat_title;?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php if(isset($search) && $search){?>
                                    <div class="col-lg-2">
                                        <button type="submit" name="excel" class="btn bg-teal-400">Export Excel<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="submit" name="pdf" class="btn bg-teal-400">Export PDF<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                <?php }?>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn bg-teal-400">Search<i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <!-- Basic pie chart -->
                    <div class="panel panel-flat">
                        
                        <div class="panel-body">
                            <?php if(count($content)>0){ 
							
									$overall_total_male = 0;
									$overall_total_female= 0;
									$overall_total_trainees = 0;
									$overall_total_complete = 0;
									$overall_total_incomplete = 0;
							?>
							<table class="table table-responsive table-bordered table-hover datatable-highlight">                        
                                <?php foreach ($content as $cat_id=>$cat_rows) { ?>
                                        <tr>
                                            <th colspan="8">
                                                <h4 class="panel-title"><?php echo $categories_list[$cat_id]?>:</h4>
                                            </th>
                                        </tr>
                                        
                                        <tr>
                                            <th>Program Name</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                            <th>Total Trainee</th>
                                            <th>Complete</th>
                                            <th>Incomplete</th>
                                            <th>Female %</th>
                                            <th>Complete %</th>
                                        </tr>
                                    
                                        <?php 
                                            $total_male = 0;
											$total_female= 0;
											$total_trainees = 0;
											$total_complete = 0;
											$total_incomplete = 0;
											$total_female_percent = 0;
											$total_complete_percent = 0;
													
                                            foreach($cat_rows as $row){
                                                $total_male += $row['male_count'];
                                                $total_female += $row['female_count'];
                                                $total_trainees += $row['male_count'] + $row['female_count'];
                                                $total_complete += $row['complete_count'];
												$total_incomplete += $row['incomplete_count'];
                                        ?>
                                        <tr>
                                            <td><?php echo ucfirst($row['title']) ?></td>
                                            <td><?php echo $row['male_count'];?></td>
                                            <td><?php echo $row['female_count'];?></td>
                                            <td><?php echo $row['male_count'] + $row['female_count'];?></td>
                                            <td><?php echo $row['complete_count'];?></td>
                                            <td><?php echo $row['incomplete_count'];?></td>
                                            <td><?php echo round( ($row['female_count'] / ($row['male_count'] + $row['female_count']) ) * 100, 2);?>%</td>
                                            <td><?php echo round( ($row['complete_count'] / ($row['complete_count'] + $row['incomplete_count']) ) * 100, 2);?>%</td>
                                        </tr>
                                        <?php } 
                                            $overall_total_male += $total_male;
											$overall_total_female += $total_female;
											$overall_total_trainees += $total_trainees;
											$overall_total_complete += $total_complete;
											$overall_total_incomplete += $total_incomplete;
                                        ?>
                                        <tr>
                                            <td colspan="8">
                                                <?php echo $categories_list[$cat_id]?> (<?php echo count($cat_rows)?> detail <?php echo (count($cat_rows) > 1) ? 'records' : 'record';?>)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdSubTitle">Sub Totals</td>
                                            <td><?php echo $total_male;?></td>
                                            <td><?php echo $total_female;?></td>
                                            <td><?php echo $total_trainees?></td>
                                            <td><?php echo $total_complete;?></td>
                                            <td><?php echo $total_incomplete;?></td>
                                            <td><?php echo round( ($total_female / ($total_female + $total_male) ) * 100, 2);?>%</td>
                                            <td><?php echo round( ($total_complete / ($total_complete + $total_incomplete) ) * 100, 2);?>%</td>
                                        </tr>
                                        <tr>
                                            <td colspan="9" style="padding-bottom:20px; border:none;">&nbsp;</td>
                                        </tr>
                                <?php } ?>
                                
                                	<tbody>
                                       
                                        <tr>
                                            <td class="tdGrandTitle">Grand Totals</td>
                                            <td><?php echo $overall_total_male;?></td>
                                            <td><?php echo $overall_total_female;?></td>
                                            <td><?php echo $overall_total_trainees?></td>
                                            <td><?php echo $overall_total_complete;?></td>
                                            <td><?php echo $overall_total_incomplete;?></td>
                                            <td><?php echo round( ($overall_total_female / ($overall_total_female + $overall_total_male) ) * 100, 2);?>%</td>
                                            <td><?php echo round( ($overall_total_complete / ($overall_total_complete + $overall_total_incomplete) ) * 100, 2);?>%</td>
                                        </tr>
                                    </tbody>
							</table>
                            
							<?php }else{?>
                                <h6>No record found</h6>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>