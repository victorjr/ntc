<div class="content">



    <!-- CKEditor default -->

    <div class="panel panel-flat">

        <div class="panel-heading">
            <h3 class="panel-title">Job Corps Report</h3>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">

                    <form method="post" class="form-horizontal" action="<?php echo(base_url('admin/reports/corps_reports')); ?>">

                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-2"> Start Date: </label>
                                <div class="col-lg-6">
                                    <input type="text" required="" name="startdate" id="datepicker" value="<?php if(isset($startdate)) echo $startdate;?>" class="form-control pickadate-max-limits">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2"> End Date: </label>
                                <div class="col-lg-6">
                                    <input type="text" required="" name="enddate" id="datepicker2" value="<?php if(isset($enddate)) echo $enddate;?>" class="form-control pickadate-max-limits">
                                </div>
                            </div>

                            <div class="form-group">
                                <?php if(isset($search) && $search){?>
                                    <div class="col-lg-2">
                                        <button type="submit" name="export" class="btn bg-teal-400">Export<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                <?php }?>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn bg-teal-400">Search<i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <!-- Basic pie chart -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h4 class="panel-title">Job Corps Report List:</h4>
                        </div>

                        <div class="panel-body">
                            <?php if(count($corps)>0){ ?>
                                <table class="table table-bordered table-hover datatable-highlight">
                                    <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Applied Date</th>
                                        <th>Trade</th>
                                        <th>Creation Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>                <?php foreach ($corps as $val) { ?>
                                        <tr>
                                            <td><?php echo ucfirst($val['firstName']) ?></td>
                                            <td><?php echo ucfirst($val['lastName']) ?></td>
                                            <td><?php echo ucfirst(date('M d, Y', (strtotime($val['dateApply'])))) ?></td>
                                            <td><?php echo ucfirst($val['tradeTitle']) ?></td>
                                            <td><?php echo ucfirst(date('M d, Y H:i', (strtotime($val['created'])))) ?></td>
                                        </tr>                <?php } ?>                </tbody>
                                </table>
                            <?php }else{?>
                                <h6>No record found</h6>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>