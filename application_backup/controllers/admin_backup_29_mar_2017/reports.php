<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Reports extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $join = array('joinTbl'=>'categories', 'on'=>'training_programs.category = categories.id','type'=>'left');
                $conditions = array('table'=>'training_programs',
                    'where'=>array('training_programs.created >='=>date('Y-m-d',strtotime($startdate)),'training_programs.created <='=>date('Y-m-d',strtotime($enddate))),
                    'fields'=>'training_programs.*,categories.title as catName',
                    'join'=>array($join));
                $content = $this->app->getData($conditions);
                if(count($content)>0){
                    $data['search'] = true;
                }

                if(isset($_POST['export']) && count($content)>0){
                    $excelHeading[] = array(
                      'title'=>'Title','contact'=>'Contact','amountRequested'=>'Amount Requested',
                      'amountApproved'=>'Amount Approved','segFund'=>'SEG Fund','nrwFund'=>'NRW Fund','organization'=>'Organization','catName'=>'Category',
                      'startDate'=>'Start Date','endDate'=>'End Date','duration'=>'Duration','location'=>'Location',
                      'established'=>'Established','academicCredit'=>'Academic Credit','certification'=>'Certification','created'=>'Created'
                    );
                    $excelData = array();
                    foreach ($content as $k=>$vl) {
                        $excelData[] = array(
                            //'id'=>$vl['id'],
                            'title'=>$vl['title'],
                            'contact'=>$vl['contact'],
                            'amountRequested'=>$vl['amountRequested'],
                            'amountApproved'=>$vl['amountApproved'],
                            'segFund'=>$vl['segFund'],
                            'nrwFund'=>$vl['nrwFund'],
                            'organization'=>$vl['organization'],
                            'location'=>$vl['location'],
                            'startDate'=>date('M d,Y',strtotime($vl['startDate'])),
                            'endDate'=>date('M d,Y',strtotime($vl['endDate'])),
                            'duration'=>$vl['duration'],
                            'catName'=>$vl['catName'],
                            'established'=>($vl['established'] == '1') ? 'Yes': 'No',
                            'academicCredit'=>($vl['academicCredit'] == '1') ? 'Yes': 'No',
                            'certification'=>($vl['certification'] == '1') ? 'Yes': 'No',
                            'created'=>date('M d,Y',strtotime($vl['created'])),
                        );
                    }
                    $excelDataSet = array_merge($excelHeading,$excelData);
                    $this->export($excelDataSet,'training_programs_report_'.date('Y-m-d').'.xls');
                }
            }
        }

        $data['programs'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/reports',$data);
        $this->load->view('admin/template/footer');

    }


    public function trainees_reports()

    {
        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $citizenJoin = array('joinTbl'=>'citizenships', 'on'=>'trainees.citizenship = citizenships.id','type'=>'left');
                $atollJoin = array('joinTbl'=>'atolls', 'on'=>'trainees.homeAtoll = atolls.id','type'=>'left');
                $conditions = array('table'=>'trainees',
                    'where'=>array('trainees.created >='=>date('Y-m-d',strtotime($startdate)),'trainees.created <='=>date('Y-m-d',strtotime($enddate))),
                    'fields'=>'trainees.*,atolls.name as atollName,citizenships.name as citizenName',
                    'join'=>array($citizenJoin,$atollJoin)
                );
                $content = $this->app->getData($conditions);
                if(count($content)>0){
                    $data['search'] = true;
                }
                //echo '<pre>';print_r($content);exit;
                if(isset($_POST['export']) && count($content)>0){
                    $excelHeading[] = array(
                        'ssn'=>'SSN','email'=>'Email',
                        'firstName'=>'First Name','lastName'=>'Last Name','gender'=>'Gender','age'=>'Age','martialStatus'=>'Martial Status',
                        'dob'=>'DOB','placeOfBirth'=>'Place Of Birth','atollName'=>'Home Atoll','citizenName'=>'citizenship',
                        'employed'=>'Employed','employedLocation'=>'Employed Location','lastSchoolAttended'=>'Last School Attended','schoolCompletedDate'=>'School Completed Date',
                        'highestGrade'=>'Highest Grade','other'=>'Other','created'=>'Created'
                    );
                    $excelData = array();
                    foreach ($content as $k=>$vl) {
                        $excelData[] = array(
                            //'id'=>$vl['id'],
                            'ssn'=>$vl['ssn'],
                            'email'=>$vl['email'],
                            'firstName'=>$vl['firstName'],
                            'lastName'=>$vl['lastName'],
                            'gender'=>$vl['gender'],
                            'age'=>$vl['age'],
                            'martialStatus'=>($vl['martialStatus'] == '1') ? 'Married': 'Single',
                            'dob'=>date('M d,Y',strtotime($vl['dob'])),
                            'placeOfBirth'=>$vl['placeOfBirth'],
                            'atollName'=>$vl['atollName'],
                            'citizenName'=>$vl['citizenName'],
                            'employed'=>($vl['employed'] == '1') ? 'Yes': 'No',
                            'employedLocation'=>$vl['employedLocation'],
                            'lastSchoolAttended'=>$vl['lastSchoolAttended'],
                            'schoolCompletedDate'=>date('M d,Y',strtotime($vl['schoolCompletedDate'])),
                            'highestGrade'=>$vl['highestGrade'],
                            'other'=>$vl['other'],
                            'created'=>date('M d,Y',strtotime($vl['created'])),
                        );
                    }
                    $excelDataSet = array_merge($excelHeading,$excelData);
                    $this->export($excelDataSet,'trainees_report_'.date('Y-m-d').'.xls');
                }
            }
        }

        $data['trainees'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/trainees_reports',$data);
        $this->load->view('admin/template/footer');

    }

    function corps_reports(){
        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $join = array('joinTbl'=>'trades', 'on'=>'job_corps.trade = trades.id','type'=>'left');
                $followJoin = array('joinTbl'=>'follow_up', 'on'=>'job_corps.id = follow_up.jobCorpId','type'=>'left');
                $conditions = array('table'=>'job_corps','where'=>array('job_corps.created >='=>date('Y-m-d',strtotime($startdate)),'job_corps.created <='=>date('Y-m-d',strtotime($enddate))),'fields'=>'job_corps.*,trades.title as tradeTitle,follow_up.enrolmentId,follow_up.id as followUpId,follow_up.jobCorpId,follow_up.followUpDate,follow_up.sentReminder','join'=>array($join,$followJoin));
                $content = $this->app->getData($conditions);
                if(count($content)>0){
                    $data['search'] = true;
                }
                //echo '<pre>';print_r($content);exit;
                if(isset($_POST['export']) && count($content)>0){
                    $excelHeading[] = array(
                        'ssn'=>'SSN', 'firstName'=>'First Name','lastName'=>'Last Name','gender'=>'Gender',
                        'dob'=>'DOB','dateApply'=>'Apply Date','intakeDate'=>'Intake Date','terminated'=>'Terminated',
                        'exitDate'=>'Exit Date','remarks'=>'Remarks','tradeTitle'=>'Trade Title','followUpDate'=>'Follow Up Date',
                        'sentReminder'=>'Sent Reminder','created'=>'Created'
                    );
                    $excelData = array();
                    foreach ($content as $k=>$vl) {
                        $excelData[] = array(
                            //'id'=>$vl['id'],
                            'ssn'=>$vl['ssn'],
                            'firstName'=>$vl['firstName'],
                            'lastName'=>$vl['lastName'],
                            'gender'=>$vl['gender'],
                            'dob'=>date('M d,Y',strtotime($vl['dob'])),
                            'dateApply'=>date('M d,Y',strtotime($vl['dateApply'])),
                            'intakeDate'=>date('M d,Y',strtotime($vl['intakeDate'])),
                            'terminated'=>($vl['terminated'] == '1') ? 'Yes': 'No',
                            'exitDate'=>date('M d,Y',strtotime($vl['exitDate'])),
                            'remarks'=>$vl['remarks'],
                            'tradeTitle'=>$vl['tradeTitle'],
                            'followUpDate'=>date('M d,Y',strtotime($vl['followUpDate'])),
                            'sentReminder'=>($vl['sentReminder'] == '1') ? 'On': 'Off',
                            'created'=>date('M d,Y',strtotime($vl['created'])),
                        );
                    }
                    $excelDataSet = array_merge($excelHeading,$excelData);
                    $this->export($excelDataSet,'job_corps_report_'.date('Y-m-d').'.xls');
                }
            }
        }

        $data['corps'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/corps_reports',$data);
        $this->load->view('admin/template/footer');
    }

    private function export($data,$filename){
       // echo '<pre>';print_r($data);exit;
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        //$this->excel->getActiveSheet()->setTitle('Users list');
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($data);
        //$filename='just_some_random_name.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');exit;
    }
}