<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Trainers extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();
    var $tbl = 'trainers';
    var $fields = 'id,firstName,lastName,username,created';

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $limit = 20;
        $conditions = array(
            'table'=>$this->tbl,
            'order'=>"id DESC"
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "firstName like '%".$q."%' OR lastName like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/trainers/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/trainers',$data);
        $this->load->view('admin/template/footer');

    }

    function programDetail($id){
        $conditions = array('table'=>$this->tbl,'where'=>array($this->tbl.'.id'=>$id),'fields'=>$this->tbl.'.*,categories.title as catName','join'=>array('joinTbl'=>'categories', 'on'=>$this->tbl.'.category = categories.id'));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/programs');
        }
        $data['data'] = $content[0];
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/programDetail',$data);
        $this->load->view('admin/template/footer');
    }

    public function addTrainer(){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('firstName', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
           // $this->form_validation->set_rules('username', 'Username', 'trim|required');
            //$this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //$file = $this->imageUpload('file');
                $data = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    //'programId' => $this->input->post('programId'),
                   // 'username' => $this->input->post('username'),
                    //'password' => sha1($this->input->post('password'))
                );

                $isAdded = $this->app->addContent($this->tbl, $data);
                if ($isAdded) {
                    $tId = $this->app->lastId();
                    $ids = array();
                    foreach ($this->input->post('programId') as $vl) {
                        $ids[] = array(
                            'programId'=>$vl,
                            'trainerId'=>$tId
                        );
                    }

                    if(count($ids)>0)
                    $this->app->insertBatch('trainers_programs',$ids);

                    $this->session->set_flashdata('success', "Trainer added successfully");
                    redirect("admin/trainers");
                } else {
                    $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }

        $training = $this->app->getData(array('table'=>'training_programs','fields'=>'id,title'));
        $data['trainings'] = $training;
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/addTrainer',$data);
        $this->load->view('admin/template/footer');
    }

    public function editTrainer($id){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('firstName', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //$file = $this->imageUpload('file');
                $data = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    //'programId' => $this->input->post('programId'),
                   // 'username' => $this->input->post('username')
                );

                /*if($this->input->post('password') != ''){
                    $data['password'] = sha1($this->input->post('password'));
                }*/

                $isUpdated = $this->app->updateRecord($this->tbl, array('id'=>$id),$data);
                //if ($isUpdated) {
                    $ids = array();
                    foreach ($this->input->post('programId') as $vl) {
                        $ids[] = array(
                            'programId'=>$vl,
                            'trainerId'=>$id
                        );
                    }

                    if(count($ids)>0) {
                        $this->app->delete('trainers_programs','trainerId',$id);
                        $this->app->insertBatch('trainers_programs', $ids);
                    }
                    $this->session->set_flashdata('success', "Trainer updated successfully");
                    redirect("admin/trainers");
                //}
                /*else {
                    $this->session->set_flashdata('success', 'Nothing Changed');
                    redirect("admin/trainers");
                }*/
            }
        }

        $conditions = array('table'=>$this->tbl,'where'=>array('id'=>$id));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/trainers');
        }

        $data['content'] = $content[0];
        $training = $this->app->getData(array('table'=>'training_programs','fields'=>'id,title'));
        $data['trainings'] = $training;

        $trainersPrograms = $this->app->getData(array('table'=>'trainers_programs','where'=>array('trainerId'=>$id),'fields'=>'id,programId'));
        $data['trainersPrograms'] = $trainersPrograms;
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/editTrainer',$data);
        $this->load->view('admin/template/footer');
    }

    function deleteTrainer($id){
        $this->app->delete($this->tbl,'id',$id);
        $this->session->set_flashdata('success', "Trainer deleted successfully");
        redirect("admin/trainers");

    }

}