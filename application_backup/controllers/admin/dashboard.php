<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Dashboard extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();



    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();
        $this->load->model('mdl_app','app');
        ini_set('display_errors', 1);
        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }
    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $programs = $this->app->getData(array(
            'table'=>'training_programs'
        ));

        $trainees = $this->app->getData(array(
            'table'=>'trainees'
        ));

        $jobCorps = $this->app->getData(array(
            'table'=>'job_corps'
        ));

        $cat = $this->app->getData(array(
            'table'=>'categories'
        ));

        $atoll = $this->app->getData(array(
            'table'=>'atolls'
        ));

        $report = array();
        foreach ($cat as $val) {
            $catPrograms = $this->app->getData(array(
                'table'=>'training_programs',
                'where'=>array('category'=>$val['id'])
            ));
            $report[$val['title']] = count($catPrograms);
        }


        $traineeReport = array();
        foreach ($atoll as $val) {
            $trainee = $this->app->getData(array(
                'table'=>'trainees',
                'where'=>array('homeAtoll'=>$val['id'])
            ));
            $traineeReport[$val['name']] = count($trainee);
        }


        /*$jobs = $this->app->getData(array(
            'table'=>'efy_jobs'
        ));

        $new = 1;
        $inProgress = 1;
        $completed = 1;
        $verified = 1;
        $graph = array();
        foreach ($jobs as $k=>$v) {
            if($v['jobstatus'] == 'new'){
                $graph[$v['jobstatus']] = $new ++;
            }else if($v['jobstatus'] == 'inprogress'){
                $graph[$v['jobstatus']] = $inProgress ++;
            }else if($v['jobstatus'] == 'completed'){
                $graph[$v['jobstatus']] = $completed ++;
            }else if($v['jobstatus'] == 'verified'){
                $graph[$v['jobstatus']] = $verified ++;
            }
        }*/
       // $data['jobsG'] = $graph;

        $data['programs'] = count($programs);
        $data['trainees'] = count($trainees);
        $data['jobCorps'] = count($jobCorps);
        $data['report'] = $report;
        $data['traineeReport'] = $traineeReport;
        //echo '<pre>';print_r(implode(',',array_keys($traineeReport)));exit;
        $this->load->view('admin/template/header');

        $this->load->view('admin/dashboard',$data);

        $this->load->view('admin/template/footer');

    }

}