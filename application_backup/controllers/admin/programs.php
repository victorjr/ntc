<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Programs extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();
    var $tbl = 'training_programs';
    var $fields = 'id,title,contact,startDate,endDate,created';
    var $categoryFields = 'id,title';
    var $category = 'categories';

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $limit = 20;
        $conditions = array(
            'table'=>$this->tbl,
            'order'=>"id DESC"
        );
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(4)){
            $page = $this->uri->segment(4);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 4;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "title like '%".$q."%' OR contact like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/programs/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();

        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->fields);
        $content = $this->app->getData($conditions);
        $data['content'] = $content;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/programs',$data);
        $this->load->view('admin/template/footer');

    }

    function programDetail($id){
        $join = array('joinTbl'=>'categories', 'on'=>$this->tbl.'.category = categories.id','type'=>'left');
        $conditions = array('table'=>$this->tbl,'where'=>array($this->tbl.'.id'=>$id),'fields'=>$this->tbl.'.*,categories.title as catName','join'=>array($join));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/programs');
        }

        $condition = array('table'=>'trainers_programs','where'=>array('programId'=>$id));
        $trainersIds = $this->app->getData($condition);
        $trainerData = '';
        foreach ($trainersIds as $k=>$v) {
            $condition = array('table'=>'trainers','where'=>array('id'=>$v['trainerId']));
            $trainers = $this->app->getData($condition);
            if(count($trainers)>0){
                $trainerData .= '<a href="'.base_url().'admin/trainers/TrainerDetail/'.$trainers[0]['id'].'">' . $trainers[0]['firstName'].' '.$trainers[0]['lastName']."</a> | ";
            }
        }

        $data['data'] = $content[0];
        $data['trainerData'] = chop($trainerData,' | ');
        //echo '<pre>';print_r($trainerData);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/programDetail',$data);
        $this->load->view('admin/template/footer');
    }

    public function addProgram(){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('contact', 'Contact', 'trim|required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //$file = $this->imageUpload('file');
                $data = array(
                    'title' => $this->input->post('title'),
                    'contact' => $this->input->post('contact'),
                    'organization' => $this->input->post('organization'),
                    'location' => $this->input->post('location'),
                    'category' => $this->input->post('category'),
                    'established' => $this->input->post('established'),
                    'academicCredit' => $this->input->post('academicCredit'),
                    'certification' => $this->input->post('certification'),
                    'nrwFund' => $this->input->post('nrwFund'),
                    'segFund' => $this->input->post('segFund'),
                    'amountApproved' => $this->input->post('amountApproved'),
                    'amountRequested' => $this->input->post('amountRequested'),
                    'duration' => $this->input->post('duration'),
                    'startDate' => $this->input->post('startDate'),
                    'endDate' => $this->input->post('endDate'),
                );

                $isAdded = $this->app->addContent($this->tbl, $data);
                if ($isAdded) {
                    $this->session->set_flashdata('success', "Program added successfully");
                    redirect("admin/programs");
                } else {
                    $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }

        $cat = $this->app->getData(array('table'=>$this->category,'fields'=>$this->categoryFields));
        $data['catList'] = $cat;
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/addProgram',$data);
        $this->load->view('admin/template/footer');
    }

    public function editProgram($id){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('contact', 'Contact', 'trim|required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //$file = $this->imageUpload('file');
                $data = array(
                    'title' => $this->input->post('title'),
                    'contact' => $this->input->post('contact'),
                    'organization' => $this->input->post('organization'),
                    'location' => $this->input->post('location'),
                    'category' => $this->input->post('category'),
                    'established' => $this->input->post('established'),
                    'academicCredit' => $this->input->post('academicCredit'),
                    'certification' => $this->input->post('certification'),
                    'nrwFund' => $this->input->post('nrwFund'),
                    'segFund' => $this->input->post('segFund'),
                    'amountApproved' => $this->input->post('amountApproved'),
                    'amountRequested' => $this->input->post('amountRequested'),
                    'duration' => $this->input->post('duration'),
                    'startDate' => $this->input->post('startDate'),
                    'endDate' => $this->input->post('endDate'),
                );

                $isUpdated = $this->app->updateRecord($this->tbl, array('id'=>$id),$data);
                if ($isUpdated) {
                    $this->session->set_flashdata('success', "Program updated successfully");
                    redirect("admin/programs");
                } else {
                    $this->session->set_flashdata('success', 'Nothing Changed');
                    redirect("admin/programs");
                }
            }
        }

        $conditions = array('table'=>$this->tbl,'where'=>array('id'=>$id));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/programs');
        }

        $data['content'] = $content[0];
        $cat = $this->app->getData(array('table'=>$this->category,'fields'=>$this->categoryFields));
        $data['catList'] = $cat;
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/editProgram',$data);
        $this->load->view('admin/template/footer');
    }

    function deleteProgram($id){
        $this->app->delete($this->tbl,'id',$id);
        $this->session->set_flashdata('success', "Program deleted successfully");
        redirect("admin/programs");

    }

}