<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Enrollments extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();
    var $tbl = 'enrollments';

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index($id)

    {
        $limit = 20;
        $programJoin = array('joinTbl'=>'training_programs', 'on'=>$this->tbl.'.programId = training_programs.id','type'=>'left');
        $traineeJoin = array('joinTbl'=>'trainees', 'on'=>$this->tbl.'.traineeId = trainees.id','type'=>'left');
        $conditions = array(
            'table'=>$this->tbl,
            'order'=>$this->tbl.".id DESC",
            'where'=>array($this->tbl.'.programId'=>$id)
        );
        $conditions = $conditions + array('join'=>array($programJoin,$traineeJoin));
        //pagination
        $this->load->library('pagination');
        if($this->uri->segment(5)){
            $page = $this->uri->segment(5);
        }else{
            $page = 1;
        }
        $config['uri_segment'] = 5;
        $config['per_page'] = $limit;

        $offset = ($page * $config['per_page']) - $config['per_page'];

        if($this->input->get('q')){
            $q = $this->input->get('q');
            $conditions['custom'] = "CONCAT(trainees.firstName,' ',trainees.lastName) like '%".$q."%'";
            $data['q'] = $q;
        }

        $total_row = $this->app->getDataCount($conditions);

        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 2;
        $config['display_pages'] = TRUE;

        // Use pagination number for anchor URL.
        $config['use_page_numbers'] = TRUE;

        $query = $_SERVER['QUERY_STRING'];
        $config['base_url'] = base_url('admin/enrollments/index');
        $config['suffix'] = '?'.$query;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $links =  $this->pagination->create_links();
        // add limit
        $conditions = $conditions + array('limit'=>$limit,'offset'=>$offset,'fields'=>$this->tbl.".*,training_programs.title,trainees.firstName,trainees.lastName");
        $content = $this->app->getData($conditions);
        $conditionsP = array('table'=>'training_programs','where'=>array('training_programs.id'=>$id));
        $contentP = $this->app->getData($conditionsP);
        if(count($contentP)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect("admin/programs");
        }
        $data['content'] = $content;
        $data['contentP'] = $contentP;
        $data['id'] = $id;
        $data['links'] = $links;
        $data['offset'] = $offset;
        $data['perPage'] = $config['per_page'];
        $data['dataInfo'] = 'Showing ' . ($offset+1) .' to '.($offset + count($content)).' of '.$total_row.' entries';
        //echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/enrollments',$data);
        $this->load->view('admin/template/footer');

    }

    function enrollmentDetail($id,$pId){
        $programJoin = array('joinTbl'=>'training_programs', 'on'=>$this->tbl.'.programId = training_programs.id','type'=>'left');
        $traineeJoin = array('joinTbl'=>'trainees', 'on'=>$this->tbl.'.traineeId = trainees.id','type'=>'left');
        $conditions = array('table'=>$this->tbl,'where'=>array($this->tbl.'.id'=>$id),'fields'=>$this->tbl.".*,training_programs.title,trainees.firstName,trainees.lastName",'join'=>array($programJoin,$traineeJoin));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect("admin/enrollments/index/" . $pId);
        }
        $data['data'] = $content[0];
        $data['id'] = $pId;
       // echo '<pre>';print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/enrollmentDetail',$data);
        $this->load->view('admin/template/footer');
    }

    public function addEnrollment($id){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('appliedDate', 'Applied Date', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                // check if user already enrolled
                $isEnrolled = $this->app->getData(array('table'=>'enrollments','fields'=>'id,programId,traineeId','where'=>array('programId'=>$id,'traineeId'=>$this->input->post('traineeId'))));
                if(count($isEnrolled)>0){
                    $this->session->set_flashdata('error', "Selected trainee is already enrolled in this program");
                    redirect($_SERVER['HTTP_REFERER']);
                }else {
                    $data = array(
                        'programId' => $id,
                        'traineeId' => $this->input->post('traineeId'),
                        'appliedDate' => $this->input->post('appliedDate'),
                        'accepted' => $this->input->post('accepted'),
                        'completed' => $this->input->post('completed'),
                        'trainingType' => $this->input->post('trainingType'),
                        'isEnrolledBefore' => $this->input->post('isEnrolledBefore'),
                        'yearOfSameTraining' => $this->input->post('yearOfSameTraining'),
                        'dayLengthOfSameTraining' => $this->input->post('dayLengthOfSameTraining')*$this->input->post('format'),
                        'format'=>$this->input->post('format'),
                        'otherPlanAfterTraining' => $this->input->post('otherPlanAfterTraining'),
                        'remarks' => $this->input->post('remarks'),
                        'willUpdateNtcOnPlans' => $this->input->post('willUpdateNtcOnPlans')
                       // 'pastProgram'=>implode(',',$this->input->post('pastProgram'))
                    );
                    if( !empty($this->input->post('pastProgram')) ){
                        $data['pastProgram'] = implode(',',$this->input->post('pastProgram'));
                    }

                    //echo '<pre>';print_r($data);exit;
                    $isAdded = $this->app->addContent($this->tbl, $data);
                    if ($isAdded) {
                        $this->session->set_flashdata('success', "Enrollment added successfully");
                        redirect("admin/enrollments/index/" . $id);

                    } else {
                        $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            }
        }

        $programs = $this->app->getData(array('table'=>'training_programs'));
        $data['programs'] = $programs;
        $trainees = $this->app->getData(array('table'=>'trainees'));
        $data['trainees'] = $trainees;
        reset($trainees);
        // get past enrolled programs of trainee
        $pastPrograms = array();
        if(count($trainees)>0){
            $firstTrainee = $trainees[0]['id'];
            $programsIds = $this->app->getData(array('table'=>'enrollments','fields'=>'id,programId,traineeId','where'=>array('traineeId'=>$firstTrainee)));
            $ids = array();
            foreach ($programsIds as $va) {
                $ids[] = $va['programId'];
            }

            $pastPrograms = $this->app->getData(array('table'=>'training_programs','fields'=>'id,title','where'=>array('endDate <='=>date("Y-m-d")),'whereIn'=>array('id'=>$ids)));
            //echo '<pre>';print_r($pastPrograms);exit;
        }
        $data['pastPrograms'] = $pastPrograms;
        $data['years'] = $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
        $data['id'] = $id;
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/addEnrollment',$data);
        $this->load->view('admin/template/footer');
    }

    public function editEnrollment($id,$pId){
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('appliedDate', 'Applied Date', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                // check if user already enrolled
                $isEnrolled = $this->app->getData(array('table'=>'enrollments','fields'=>'id,programId,traineeId','where'=>array('id !='=>$id,'programId'=>$pId,'traineeId'=>$this->input->post('traineeId'))));
                if(count($isEnrolled)>0){
                    $this->session->set_flashdata('error', "Selected trainee is already enrolled in this program");
                    redirect($_SERVER['HTTP_REFERER']);
                }else {
                    $data = array(
                        'programId' => $pId,
                        //'traineeId' => $this->input->post('traineeId'),
                        'appliedDate' => $this->input->post('appliedDate'),
                        'accepted' => $this->input->post('accepted'),
                        'completed' => $this->input->post('completed'),
                        'trainingType' => $this->input->post('trainingType'),
                        'isEnrolledBefore' => $this->input->post('isEnrolledBefore'),
                        'yearOfSameTraining' => $this->input->post('yearOfSameTraining'),
                        'dayLengthOfSameTraining' => $this->input->post('dayLengthOfSameTraining')*$this->input->post('format'),
                        'format'=>$this->input->post('format'),
                        'otherPlanAfterTraining' => $this->input->post('otherPlanAfterTraining'),
                        'remarks' => $this->input->post('remarks'),
                        'willUpdateNtcOnPlans' => $this->input->post('willUpdateNtcOnPlans'),
                        'pastProgram'=>''
                    );

                    if( !empty($this->input->post('pastProgram')) ){
                        $data['pastProgram'] = implode(',',$this->input->post('pastProgram'));
                    }
                    //echo '<pre>';print_r($data);exit;

                    $isUpdated = $this->app->updateRecord($this->tbl, array('id' => $id), $data);
                    if ($isUpdated) {
                        $this->session->set_flashdata('success', "Enrollment updated successfully");
                        redirect("admin/enrollments/index/" . $pId);
                    } else {
                        $this->session->set_flashdata('success', 'Nothing Changed');
                        redirect("admin/enrollments/index/" . $pId);
                    }
                }
            }
        }

        $conditions = array('table'=>$this->tbl,'where'=>array('id'=>$id));
        $content = $this->app->getData($conditions);
        if(count($content)<=0){
            $this->session->set_flashdata('error', 'No record found');
            redirect('admin/programs');
        }

        $data['content'] = $content[0];
        $programs = $this->app->getData(array('table'=>'training_programs'));
        $data['programs'] = $programs;
        $trainees = $this->app->getData(array('table'=>'trainees'));
        $data['trainees'] = $trainees;
        reset($trainees);
        // get past enrolled programs of trainee
        $pastPrograms = array();
        if(count($trainees)>0){
            $firstTrainee = $trainees[0]['id'];
            $programsIds = $this->app->getData(array('table'=>'enrollments','fields'=>'id,programId,traineeId','where'=>array('traineeId'=>$firstTrainee)));
            $ids = array();
            foreach ($programsIds as $va) {
                $ids[] = $va['programId'];
            }

            $pastPrograms = $this->app->getData(array('table'=>'training_programs','fields'=>'id,title','where'=>array('endDate <='=>date("Y-m-d")),'whereIn'=>array('id'=>$ids)));
            //echo '<pre>';print_r($pastPrograms);exit;
        }
        $data['pastPrograms'] = $pastPrograms;
        $data['years'] = $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
        $data['id'] = $pId;
//        print_r($data);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/editEnrollment',$data);
        $this->load->view('admin/template/footer');
    }

    function deleteEnrollment($id,$pId){
        $this->app->delete($this->tbl,'id',$id);
        $this->session->set_flashdata('success', "Enrollment deleted successfully");
        redirect("admin/enrollments/index/".$pId);

    }

}