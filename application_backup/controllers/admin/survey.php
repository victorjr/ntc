<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Survey extends CI_Controller

{
    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app', 'app');

    }

    function reminderCheck(){
        $currentDate = date('Y-m-d H:i',strtotime(date('Y-m-d H:i').' -3 minute'));
        $join = array('joinTbl' => 'follow_up', 'on' => 'job_corps.id = follow_up.jobCorpId', 'type' => 'left');
        $corps = $this->app->getData(array('fields' => 'job_corps.*,follow_up.*','join'=>array($join),'table'=>'job_corps','where'=>array('follow_up.sentReminder'=>1,'job_corps.isSent'=>0,'DATE_FORMAT(job_corps.created,"%Y-%m-%d %H:%i") = '=>/*'2017-01-20 06:50'*/$currentDate)));
        foreach ($corps as $val) {
            $trainee = $this->app->getData(array('table'=>'enrollments','fields'=>array('traineeId'),'where'=>array('id'=>$val['enrolmentId'])));
            if(count($trainee)>0){
                $traineeDetail = $this->app->getData(array('table'=>'trainees','where'=>array('id'=>$trainee[0]['traineeId'])));
                if(count($traineeDetail)>0 && !empty($traineeDetail[0]['email'])) {
                    // send
                    $token = generateCode();
                     $this->app->addContent('tokens', array(
                        'token'=>$token,
                        'enrolmentId'=>$val['enrolmentId'],
                        'traineeId'=>$trainee[0]['traineeId']
                    ));
                    $link = "http://ntc.matrix-intech.com/admin/survey/add/".$token;
                    $mess = 'Hello '.$traineeDetail[0]['firstName'].' '.$traineeDetail[0]['lastName'].",\n\n Please use the link ".$link." for questionnaire.";
                    $this->sendEmail($traineeDetail[0]['email'],$mess);
                    $this->app->updateRecord('job_corps', array('id' => $val['jobCorpId']), array('isSent' => 1));
                    echo '<pre>';print_r($traineeDetail);
                }
            }
        }


        echo '<pre>';print_r($corps);
        echo $currentDate.'  ';
        echo date("Y/m/d H:i:s");
        exit;
    }

    function sendEmail($email,$message){
        $sender_email = "ntc@info.com";
        $receiver_email = $email;
        $username = "NTC";
        $subject = "NTC Trace Study Follow Up Questionnaire";

        // Load email library and passing configured values to email library
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        // Sender email address
        $this->email->from($sender_email, $username);
        // Receiver email address
        $this->email->to($receiver_email);
        // Subject of email
        $this->email->subject($subject);
        // Message in email
        $this->email->message($message);

        if ($this->email->send()) {

        }
    }
    function index()
    {
        $data = array(
            'ssn' => 'asdf'
        );
        $isAdded = $this->app->addContent('trainees', $data);
        exit;
    }

    function add($token){

        $tokens = $this->app->getData(array('table'=>'tokens','where'=>array('token'=>$token)));
        if(count($tokens)<=0){
            echo 'Invalid or expired token';exit;
        }


        $enrolment = $tokens[0]['enrolmentId'];
        $traineeId = $tokens[0]['traineeId'];
        // get trainee code
        $user = $this->app->getData(array('table'=>'trainees','where'=>array('id'=>$traineeId)));
        if(count($user)<=0){
            // error
            redirect('admin/login');
        }

        $join = array('joinTbl' => 'follow_up', 'on' => 'job_corps.id = follow_up.jobCorpId', 'type' => 'left');
        $conditions = array('table' => 'job_corps', 'where' => array('follow_up.enrolmentId'=>$enrolment,'job_corps.ssn' => $user[0]['ssn']), 'fields' => 'job_corps.*,follow_up.*', 'join' => array($join));
        $content = $this->app->getData($conditions);

        if(count($content)<=0){
            // error
            redirect('admin/login');
        }

        $jobCorpId = $content[0]['jobCorpId'];

        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //echo '<pre>';print_r($_POST);exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('graduated', 'Graduated', 'trim|required');
            //$this->form_validation->set_rules('why_did_you_not_graduated', 'Why did you not graduated?', 'trim|required');
            $this->form_validation->set_rules('what_are_you_doing_now', 'What are you doing now', 'trim|required');
            $this->form_validation->set_rules('working', 'Working', 'trim|required');
            //$this->form_validation->set_rules('why_are_you_not_working', 'Why are you not working?', 'trim|required');
            $this->form_validation->set_rules('did_employer_offer_job', 'Did employer offer job', 'trim|required');
            //$this->form_validation->set_rules('no_offer_why', 'If not offer job why', 'trim|required');
            $this->form_validation->set_rules('who_is_employer', 'Who is your employer', 'trim|required');
            $this->form_validation->set_rules('self_employed', 'Self Employed', 'trim|required');
            $this->form_validation->set_rules('work_location', 'Workplace Location', 'trim|required');
            $this->form_validation->set_rules('job_title', 'Job Title', 'trim|required');
            $this->form_validation->set_rules('using_skills_learning_in_training', 'Using skills learning in training', 'trim|required');
            $this->form_validation->set_rules('ever_been_promoted', 'Ever been promoted', 'trim|required');
            $this->form_validation->set_rules('earning_more_than_before_training', 'Earning more than before training', 'trim|required');
            $this->form_validation->set_rules('already_or_intend_to_enroll_in_other_training', 'Already or intend to enroll in other training', 'trim|required');
            $this->form_validation->set_rules('if_yes_what_type', 'If yes what type', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                // redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                //echo '<pre>';print_r($_POST);exit;
                $dataS = array(
                    'enrolmentId'=>$enrolment,
                    'jobCorpId'=>$jobCorpId,
                    'traineeId'=>$traineeId,
                    'graduated' => $this->input->post('graduated'),
                    'why_did_you_not_graduated' => $this->input->post('why_did_you_not_graduated'),
                    'what_are_you_doing_now' => $this->input->post('what_are_you_doing_now'),
                    'working' => $this->input->post('working'),
                    'why_are_you_not_working' => $this->input->post('why_are_you_not_working'),
                    'did_employer_offer_job' => $this->input->post('did_employer_offer_job'),
                    'no_offer_why' => $this->input->post('no_offer_why'),
                    'who_is_employer' => $this->input->post('who_is_employer'),
                    'self_employed' => $this->input->post('self_employed'),
                    'work_location' => $this->input->post('work_location'),
                    'job_title' => $this->input->post('job_title'),
                    'using_skills_learning_in_training' => $this->input->post('using_skills_learning_in_training'),
                    'ever_been_promoted' => $this->input->post('ever_been_promoted'),
                    'earning_more_than_before_training' => $this->input->post('earning_more_than_before_training'),
                    'already_or_intend_to_enroll_in_other_training' => $this->input->post('already_or_intend_to_enroll_in_other_training'),
                    'if_yes_what_type' => $this->input->post('if_yes_what_type'),
                );
                $isAdded = $this->app->addContent('tracer_study', $dataS);
                if ($isAdded) {
                    $this->app->delete('tokens', 'id', $tokens[0]['id']);
                    /*$this->session->set_flashdata('success', "Thanks for your feedback");
                    redirect($_SERVER['HTTP_REFERER']);*/
                    echo '<h2>Thanks for your support</h2>';exit;
                } else {
                    $this->session->set_flashdata('error', 'Something Went Wrong... Try Again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }

        $data['token'] = $token;
        $data['jobCorpId'] = $jobCorpId;
        $data['traineeId'] = $traineeId;
        $this->load->view('admin/form',$data);
    }
}