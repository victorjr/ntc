<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Dompdf\Dompdf;


class Reports extends CI_Controller

{

    /////////////////////////////////////

    ////////// DECLARING VARIABLES //////

    /////////////////////////////////////

    var $data = array();

    /////////////////////////////////////

    ////////// CONSTRUCTOR //////////////

    /////////////////////////////////////

    function __construct()

    {

        parent::__construct();

        ini_set('display_errors', 1);

        $this->load->model('mdl_app','app');

        if(!$this->session->userdata('user')){
            $this->session->set_flashdata('error','Login to view page');
            redirect(base_url('login'));
        }

    }



    /////////////////////////////////////

    ////////// INDEX FUNCTION ///////////

    /////////////////////////////////////



    public function index()

    {
        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $join = array('joinTbl'=>'categories', 'on'=>'training_programs.category = categories.id','type'=>'left');
                $conditions = array('table'=>'training_programs',
                    'where'=>array('training_programs.created >='=>date('Y-m-d',strtotime($startdate)),'training_programs.created <='=>date('Y-m-d',strtotime($enddate))),
                    'fields'=>'training_programs.*,categories.title as catName',
                    'join'=>array($join));
                $content = $this->app->getData($conditions);
                if(count($content)>0){
                    $data['search'] = true;
                }

                if(isset($_POST['export']) && count($content)>0){
                    $excelHeading[] = array(
                      'title'=>'Title','contact'=>'Contact','amountRequested'=>'Amount Requested',
                      'amountApproved'=>'Amount Approved','segFund'=>'SEG Fund','nrwFund'=>'NRW Fund','organization'=>'Organization','catName'=>'Category',
                      'startDate'=>'Start Date','endDate'=>'End Date','duration'=>'Duration','location'=>'Location',
                      'established'=>'Established','academicCredit'=>'Academic Credit','certification'=>'Certification','created'=>'Created'
                    );
                    $excelData = array();
                    foreach ($content as $k=>$vl) {
                        $excelData[] = array(
                            //'id'=>$vl['id'],
                            'title'=>$vl['title'],
                            'contact'=>$vl['contact'],
                            'amountRequested'=>$vl['amountRequested'],
                            'amountApproved'=>$vl['amountApproved'],
                            'segFund'=>$vl['segFund'],
                            'nrwFund'=>$vl['nrwFund'],
                            'organization'=>$vl['organization'],
                            'location'=>$vl['location'],
                            'startDate'=>date('M d,Y',strtotime($vl['startDate'])),
                            'endDate'=>date('M d,Y',strtotime($vl['endDate'])),
                            'duration'=>$vl['duration'],
                            'catName'=>$vl['catName'],
                            'established'=>($vl['established'] == '1') ? 'Yes': 'No',
                            'academicCredit'=>($vl['academicCredit'] == '1') ? 'Yes': 'No',
                            'certification'=>($vl['certification'] == '1') ? 'Yes': 'No',
                            'created'=>date('M d,Y',strtotime($vl['created'])),
                        );
                    }
                    $excelDataSet = array_merge($excelHeading,$excelData);
                    $this->export($excelDataSet,'training_programs_report_'.date('Y-m-d').'.xls');
                }
            }
        }

        $data['programs'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/reports',$data);
        $this->load->view('admin/template/footer');

    }


    public function trainees_reports()

    {
        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $citizenJoin = array('joinTbl'=>'citizenships', 'on'=>'trainees.citizenship = citizenships.id','type'=>'left');
                $atollJoin = array('joinTbl'=>'atolls', 'on'=>'trainees.homeAtoll = atolls.id','type'=>'left');
                $conditions = array('table'=>'trainees',
                    'where'=>array('trainees.created >='=>date('Y-m-d',strtotime($startdate)),'trainees.created <='=>date('Y-m-d',strtotime($enddate))),
                    'fields'=>'trainees.*,atolls.name as atollName,citizenships.name as citizenName',
                    'join'=>array($citizenJoin,$atollJoin)
                );
                $content = $this->app->getData($conditions);
                if(count($content)>0){
                    $data['search'] = true;
                }
                //echo '<pre>';print_r($content);exit;
                if(isset($_POST['export']) && count($content)>0){
                    $excelHeading[] = array(
                        'ssn'=>'SSN','email'=>'Email',
                        'firstName'=>'First Name','lastName'=>'Last Name','gender'=>'Gender','age'=>'Age','martialStatus'=>'Martial Status',
                        'dob'=>'DOB','placeOfBirth'=>'Place Of Birth','atollName'=>'Home Atoll','citizenName'=>'citizenship',
                        'employed'=>'Employed','employedLocation'=>'Employed Location','lastSchoolAttended'=>'Last School Attended','schoolCompletedDate'=>'School Completed Date',
                        'highestGrade'=>'Highest Grade','other'=>'Other','created'=>'Created'
                    );
                    $excelData = array();
                    foreach ($content as $k=>$vl) {
                        $excelData[] = array(
                            //'id'=>$vl['id'],
                            'ssn'=>$vl['ssn'],
                            'email'=>$vl['email'],
                            'firstName'=>$vl['firstName'],
                            'lastName'=>$vl['lastName'],
                            'gender'=>$vl['gender'],
                            'age'=>$vl['age'],
                            'martialStatus'=>($vl['martialStatus'] == '1') ? 'Married': 'Single',
                            'dob'=>date('M d,Y',strtotime($vl['dob'])),
                            'placeOfBirth'=>$vl['placeOfBirth'],
                            'atollName'=>$vl['atollName'],
                            'citizenName'=>$vl['citizenName'],
                            'employed'=>($vl['employed'] == '1') ? 'Yes': 'No',
                            'employedLocation'=>$vl['employedLocation'],
                            'lastSchoolAttended'=>$vl['lastSchoolAttended'],
                            'schoolCompletedDate'=>date('M d,Y',strtotime($vl['schoolCompletedDate'])),
                            'highestGrade'=>$vl['highestGrade'],
                            'other'=>$vl['other'],
                            'created'=>date('M d,Y',strtotime($vl['created'])),
                        );
                    }
                    $excelDataSet = array_merge($excelHeading,$excelData);
                    $this->export($excelDataSet,'trainees_report_'.date('Y-m-d').'.xls');
                }
            }
        }

        $data['trainees'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/trainees_reports',$data);
        $this->load->view('admin/template/footer');

    }

    function corps_reports(){
        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $join = array('joinTbl'=>'trades', 'on'=>'job_corps.trade = trades.id','type'=>'left');
                $followJoin = array('joinTbl'=>'follow_up', 'on'=>'job_corps.id = follow_up.jobCorpId','type'=>'left');
                $conditions = array('table'=>'job_corps','where'=>array('job_corps.created >='=>date('Y-m-d',strtotime($startdate)),'job_corps.created <='=>date('Y-m-d',strtotime($enddate))),'fields'=>'job_corps.*,trades.title as tradeTitle,follow_up.enrolmentId,follow_up.id as followUpId,follow_up.jobCorpId,follow_up.followUpDate,follow_up.sentReminder','join'=>array($join,$followJoin));
                $content = $this->app->getData($conditions);
                if(count($content)>0){
                    $data['search'] = true;
                }
                //echo '<pre>';print_r($content);exit;
                if(isset($_POST['export']) && count($content)>0){
                    $excelHeading[] = array(
                        'ssn'=>'SSN', 'firstName'=>'First Name','lastName'=>'Last Name','gender'=>'Gender',
                        'dob'=>'DOB','dateApply'=>'Apply Date','intakeDate'=>'Intake Date','terminated'=>'Terminated',
                        'exitDate'=>'Exit Date','remarks'=>'Remarks','tradeTitle'=>'Trade Title','followUpDate'=>'Follow Up Date',
                        'sentReminder'=>'Sent Reminder','created'=>'Created'
                    );
                    $excelData = array();
                    foreach ($content as $k=>$vl) {
                        $excelData[] = array(
                            //'id'=>$vl['id'],
                            'ssn'=>$vl['ssn'],
                            'firstName'=>$vl['firstName'],
                            'lastName'=>$vl['lastName'],
                            'gender'=>$vl['gender'],
                            'dob'=>date('M d,Y',strtotime($vl['dob'])),
                            'dateApply'=>date('M d,Y',strtotime($vl['dateApply'])),
                            'intakeDate'=>date('M d,Y',strtotime($vl['intakeDate'])),
                            'terminated'=>($vl['terminated'] == '1') ? 'Yes': 'No',
                            'exitDate'=>date('M d,Y',strtotime($vl['exitDate'])),
                            'remarks'=>$vl['remarks'],
                            'tradeTitle'=>$vl['tradeTitle'],
                            'followUpDate'=>date('M d,Y',strtotime($vl['followUpDate'])),
                            'sentReminder'=>($vl['sentReminder'] == '1') ? 'On': 'Off',
                            'created'=>date('M d,Y',strtotime($vl['created'])),
                        );
                    }
                    $excelDataSet = array_merge($excelHeading,$excelData);
                    $this->export($excelDataSet,'job_corps_report_'.date('Y-m-d').'.xls');
                }
            }
        }

        $data['corps'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/corps_reports',$data);
        $this->load->view('admin/template/footer');
    }

    //Program Summary
     function program_summary(){

        $conditions = array('table'=>'categories',);
        $categories = $this->app->getData($conditions);

        $categories_list = array();
        foreach($categories as $ct){
            $categories_list[$ct['id']] = $ct['title'];
        }

        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $category_id = $this->input->post('category_id');

                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $data['category_id'] = $category_id;

                $conditions = array(
                    'table'=>'training_programs',
                    'where'=>array(
                        'training_programs.startDate >='=>date('Y-m-d',strtotime($startdate)),
                        'training_programs.endDate <='=>date('Y-m-d',strtotime($enddate))
                    ),
                    'order'=>'training_programs.category'
                );

                if($category_id){
                    $conditions['where']['training_programs.category'] = $category_id;
                }

                $tmpcontent = $this->app->getData($conditions);
                
                foreach($tmpcontent as $ct){

                    $sjoin = array(
                        'joinTbl'=>'trainees', 
                        'on'=>'enrollments.traineeId = trainees.id',
                        'type'=>'left'
                    );


                    $conditions = array(
                        'table'=>'enrollments',
                        'join'=>array($sjoin),
                        'fields'=>'enrollments.*, trainees.gender',
                        'where'=>array(
                            'enrollments.programId'=>$ct['id']
                        )
                    );

                    $trainees = $this->app->getData($conditions);
                    $male_count = 0;
                    $female_count = 0;
                
                    foreach($trainees as $tra){
                        if($tra['gender'] == 'female'){
                            $female_count++;
                        }else{
                            $male_count++;
                        }
                    }

                    $ct['male_count'] = $male_count;
                    $ct['female_count'] = $female_count;
                    $ct['total_trainees'] = $female_count + $male_count;
                    
                    $content[$ct['category']][] = $ct;
                }

                if(count($content) > 0){
                    $data['search'] = true;
                }
                
                if(isset($_POST['excel']) && count($content) > 0){

                    $html = $this->load->view('admin/reports_excel/program_summary', array('startdate'=>date('m/d/Y', strtotime($startdate)), 'enddate'=>date('m/d/Y', strtotime($enddate)), 'content'=>$content, 'categories'=>$categories_list), true);
                    $html = str_replace('&', '&amp;', $html);

                    $this->export_html($html, 'program_summary_'.date('Y-m-d').'.xls');

                }

                if(isset($_POST['pdf']) && count($content) > 0){

                    $html = $this->load->view('admin/reports_excel/program_summary', array('startdate'=>date('m/d/Y', strtotime($startdate)), 'enddate'=>date('m/d/Y', strtotime($enddate)), 'content'=>$content, 'categories'=>$categories_list), true);
                    $html = str_replace('&', '&amp;', $html);

                    $this->export_html_to_pdf($html, 'program_summary_'.date('Y-m-d').'.pdf');

                }

            }
        }

        $data['categories_list'] = $categories_list;
        $data['content'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/program_summary',$data);
        $this->load->view('admin/template/footer');
    }

    //Training Summary
     function training_summary(){

        $conditions = array('table'=>'categories',);
        $categories = $this->app->getData($conditions);

        $categories_list = array();
        foreach($categories as $ct){
            $categories_list[$ct['id']] = $ct['title'];
        }

        $content = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('startdate', 'Start', 'trim|required');
            $this->form_validation->set_rules('enddate', 'End', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
            else {
                $startdate = $this->input->post('startdate');
                $enddate = $this->input->post('enddate');
                $category_id = $this->input->post('category_id');

                $data['enddate'] = $enddate;
                $data['startdate'] = $startdate;
                $data['category_id'] = $category_id;

                $join = array(
                    'joinTbl'=>'categories', 
                    'on'=>'training_programs.category = categories.id',
                    'type'=>'left'
                );

                $conditions = array(
                    'table'=>'training_programs',
                    //'join'=>array($join),
                    //'fields'=>'training_programs.*,categories.title as category_name',
                    'where'=>array(
                        //'training_programs.startDate >='=>date('Y-m-d',strtotime($startdate)),
                        //'training_programs.endDate <='=>date('Y-m-d',strtotime($enddate))
                    ),
                    'order'=>'training_programs.category'
                );

                if($category_id){
                    $conditions['where']['training_programs.category'] = $category_id;
                }

                $tmpcontent = $this->app->getData($conditions);
                
                foreach($tmpcontent as $ct){

                    $sjoin = array(
                        'joinTbl'=>'trainees', 
                        'on'=>'enrollments.traineeId = trainees.id',
                        'type'=>'left'
                    );


                    $conditions = array(
                        'table'=>'enrollments',
                        'join'=>array($sjoin),
                        'fields'=>'enrollments.*, trainees.gender',
                        'where'=>array(
                            'enrollments.programId'=>$ct['id']
                        )
                    );

                    $conditions = array(
                        'table'=>'enrollments',
                        'join'=>array($sjoin),
                        'fields'=>'enrollments.*, trainees.gender',
                        'where'=>array(
                            'enrollments.appliedDate >='=>date('Y-m-d',strtotime($startdate)),
                            'enrollments.appliedDate <='=>date('Y-m-d',strtotime($enddate)),
                            'enrollments.programId'=>$ct['id']
                        )
                    );

                    $trainees = $this->app->getData($conditions);

                    //echo '<pre>';print_r($trainees);
                    $male_count = 0;
                    $female_count = 0;
                    $complete_count = 0;
                    $incomplete_count = 0;
                    foreach($trainees as $tra){
                        if($tra['gender'] == 'female'){
                            $female_count++;
                        }else{
                            $male_count++;
                        }

                        if( !empty($tra['completed']) ){
                            $complete_count++;
                        }else{
                            $incomplete_count++;
                        }

                    }
                    
                    if($male_count == 0 && $female_count == 0){
                        continue;
                    }

                    $ct['male_count'] = $male_count;
                    $ct['female_count'] = $female_count;
                    $ct['complete_count'] = $complete_count;
                    $ct['incomplete_count'] = $incomplete_count;
                    

                    $content[$ct['category']][] = $ct;
                }
                
                //echo '<pre>';print_r($content);exit;

                if(count($content) > 0){
                    $data['search'] = true;
                }
                
                if(isset($_POST['excel']) && count($content) > 0){

                    $html = $this->load->view('admin/reports_excel/training_summary', array('startdate'=>date('m/d/Y', strtotime($startdate)), 'enddate'=>date('m/d/Y', strtotime($enddate)), 'content'=>$content, 'categories'=>$categories_list), true);
                    $html = str_replace('&', '&amp;', $html);

                    $this->export_html($html, 'training_summary_'.date('Y-m-d').'.xls');
                }

                if(isset($_POST['pdf']) && count($content) > 0){

                    $html = $this->load->view('admin/reports_excel/training_summary', array('startdate'=>date('m/d/Y', strtotime($startdate)), 'enddate'=>date('m/d/Y', strtotime($enddate)), 'content'=>$content, 'categories_list'=>$categories_list), true);
                    $html = str_replace('&', '&amp;', $html);

                    $this->export_html_to_pdf($html, 'training_summary_'.date('Y-m-d').'.pdf');

                }

            }
        }

        $data['categories_list'] = $categories_list;
        $data['content'] = $content;
        //echo '<pre>';print_r($content);exit;
        $this->load->view('admin/template/header');
        $this->load->view('admin/training_summary',$data);
        $this->load->view('admin/template/footer');
    }

    private function export($data,$filename){
       // echo '<pre>';print_r($data);exit;
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        //$this->excel->getActiveSheet()->setTitle('Users list');
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($data);
        //$filename='just_some_random_name.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');exit;
    }

    private function export_html($html, $filename){
        $this->load->library('excel');

        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);

        // Read the contents of the file into PHPExcel Reader class
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile);

        unlink($tmpfile);

        header('Content-Type: text/html; charset=utf-8');
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        // Pass to writer and output as needed
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
        $objWriter->save('php://output');exit;


    }

    private function export_html_to_pdf($html, $filename){
        //echo $_SERVER['DOCUMENT_ROOT'].'/application/libraries/dompdf/autoload.inc.php';
        require_once $_SERVER['DOCUMENT_ROOT'].'/application/libraries/dompdf/autoload.inc.php';

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

    }
}