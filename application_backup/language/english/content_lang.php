<?php
/******************
 *
 * Text strings
 *
 */
    $lang['welcome_message'] = 'Welcome To DMS';
    $lang['home'] = 'Home';
    $lang['about-us'] = 'About Us';
    $lang['who-we-are'] = 'Who we are';
    $lang['vision'] = 'Vision';
    $lang['mission'] = 'Mission';
    $lang['team'] = 'Team';
    $lang['projects&publications'] = 'Projects & Publications';
    $lang['projects'] = 'Projects';
    $lang['financial-reports'] = 'Financial Reports';
    $lang['performance-reports'] = 'Performance Reports';
    $lang['be-prepared'] = 'Be Prepared';
    $lang['training-videos'] = 'Training Videos';
    $lang['emergency-plans'] = 'Emergency Videos';
    $lang['training-courses'] = 'Training Courses';
    $lang['warning&alerts'] = 'Alerts & Warnings';
    $lang['make-appeal'] = 'Make Appeal';
    $lang['rehabilitation'] = 'Rehabilitation';
    $lang['get-involved'] = 'Get Involved';
    $lang['be-a-volunteer'] = 'Be a volunteer';
    $lang['donation'] = 'Donation';
    $lang['disaster-information'] = 'Disaster Information';
?>